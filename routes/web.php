<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::group(['prefix' => 'system'], function () {
    Auth::routes();
});

Route::get('/admin', 'HomeController@index')->name('admin')->middleware('auth')->middleware('rbac');



Route::group(['prefix' => 'visitor'], function () {



    // routes for visitor login
    Route::get('login', 'Auth\VisitorLoginController@showLoginForm')->name('visitor.login');
    Route::post('post-login', 'Auth\VisitorLoginController@login');

     // login with github
     Route::get('/login/github', 'Auth\VisitorLoginController@redirectToProvider');
     Route::get('/login/github/callback', 'Auth\VisitorLoginController@handleProviderCallback');


     Route::get('/login/facebook', 'Auth\VisitorLoginController@redirectToProvider');
     Route::get('/login/facebook/callback', 'Auth\VisitorLoginController@handleProviderCallback');

    Route::get('/', 'VisitorController@index')->name('visitor.dashboard');

    Route::post('/visitor-logout','VisitorController@logout')->name('visitor.logout');
});


// to get current location

Route::get('location', function () {



    $ip = request()->ip();

    $ip = '27.34.21.50' ;

    // dd($ip);

    $data = \Location::get($ip);

    // dd($data);

    $customer_data = [
        $data->cityName,
        $data->regionName,
        $data->countryName
    ];

    dd($customer_data);


});
