<!DOCTYPE html>
<html lang="zxx">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Aamul Place to buy & Sell</title>
    
    <!--Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset('public/front/css/bootstrap.min.css')}}">

    <!--Font Awesome CSS-->
    <link rel="stylesheet" href="{{asset('public/front/css/all.css')}}">

    <!--Main CSS-->
    <link rel="stylesheet" href="{{asset('public/front/css/style.css')}}">


    <link rel="stylesheet" type="text/css" href="{{asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}">

    <link href="{{asset('https://fonts.googleapis.com/css?family=Roboto:400,500,700,900&display=swap')}}" rel="stylesheet">

    <link href="{{asset('public/front/css/main.css')}}" rel="stylesheet" />

    <link rel="stylesheet" href="{{asset('public/front/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/front/css/owl.theme.default.min.css')}}">

    {{-- Date picker --}}
    <link rel="stylesheet" href="{{asset('public/bootstrap-datepicker/bootstrap-datepicker3.css')}}">


</head>
<body class="js">

    @include('layouts.front.header')

        @yield('content')



	<!-- Start Footer Area -->
	@include('layouts.front.footer')
	<!-- /End Footer Area -->

<script src="{{asset('https://code.jquery.com/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('public/front/js/bootstrap.min.js')}}"></script>    
<script src="{{asset('public/front/js/popper.min.js')}}"></script> 
<script src="{{asset('public/front/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('public/front/js/custom.js')}}"></script>
<script src="{{asset('public/front/js/main.js')}}"></script>


    {{-- Preview image --}}

    <script src="{{ asset('public/js/preview_image.js') }}">

    </script>

    {{-- date picker --}}

    <script src="{{asset('public/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>


    {{-- Toaster --}}

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


{{-- Date format --}}
    <script>
           $('*[data-provide="datepicker"]').datepicker({
        format: 'dd-mm-yyyy',
        todayHighlight:'TRUE',
        autoclose: true,
    })
    .on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    </script>



<script type="text/javascript">


    @if(\Session::has('msg'))

              toastr.success("{{ \Session::get('msg') }}");
              @endif
              @if($errors->any())

                      @foreach($errors->all() as $e)
              toastr.error("{{ $e }}");

                      @endforeach

              @endif
              window.onscroll = function() {myFunction()};

  // Get the header
  var header = document.getElementById("myHeader");

  // Get the offset position of the navbar
  var sticky = header.offsetTop;

  // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
  function myFunction() {
    if (window.pageYOffset > sticky) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  }



  </script>
</body>
</html>
