@php
    // dd(Auth::guard('visitor'));
    // dd($main_categories);
@endphp


	<!-- Header -->
	<header>
  <div class="top-header">
    <div class="container">
      <div class="row">
        <div class="col-4 col-sm-6 col-md-8 col-lg-8">
          <nav class="navbar navbar-expand-lg navbar-light">
              <!-- <a class="navbar-brand" href="index.html"><img src="./images/logo.png" alt=""></a> -->
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fa fa-home"></i></a>
                  </li>
                  @foreach ($categories as $value)
                  <li class="dropdown mega-dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $value->main_category_id}} <span class="caret"></span></a>        
                    <div class="dropdown-menu mega-dropdown-menu">
                      <div class="row">
                        <div class="col-md-6">
                          <ul>
                            <li><a href="#">{{$value->category_name}}</a></li>
                          </ul>
                        </div>
                        <div class="col-md-6">
                          <ul>
                            <li><a href="#">Auto Carousel</a></li>
                            <li><a href="#">Carousel Control</a></li>
                            <li><a href="#">Left & Right Navigation</a></li>
                            <li><a href="#">Four Columns Grid</a></li>
                          </ul>
                        </div>
                      </div>
                    </div>        
                  </li>
                  @endforeach
                  {{-- <li class="nav-item">
                    <a class="nav-link" href="#"></a>
                  </li> --}}
                  
                  {{-- <li class="nav-item">
                    <a class="nav-link" href="#">Jobs</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Real Estate</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Service For Hire</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">Value mY Car</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">More...</a>
                  </li> --}}
                </ul>
              </div>
            </nav>
        </div>
        <div class="col-8 col-sm-6 col-md-4 col-lg-4 signin-right">
          <ul>
            <li><a href="#">Sign In</a></li>
            <li><a href="#">Register</a></li>
            <li><a href="#"><i class="fa fa-user"></i> My Gumtree</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="logo-section">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
          <a href="#" class="logo"><img src="{{asset('public/front/./images/logo1.png')}}" alt=""></a>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-lg-6">
          <a href="#" class="btn btn-post">Post an ad</a>
        </div>
      </div>
    </div>
  </div>
  <div class="mobile-header">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <span class="clickmenus" onclick="openNav()">&#9776; </span>
          <div id="mySidenav" class="sidenav">
            <a href="{{asset('javascript:void(0)')}}" class="closebtn" onclick="closeNav()">&times;</a>
            <div class="mobile-menus">
              <ul>
                <li><a href="#">Home</a></li>
                <li>
                  <a href="#" type="text" data-toggle="collapse" data-target="#multiCollapseExample2" aria-expanded="false" aria-controls="multiCollapseExample2">Cars & Vehicles <i class="wsmenu-arrow fa fa-angle-down"></i></a>
                  <div class="collapse multi-collapse" id="multiCollapseExample2">
                    <div class="card card-body">
                    <ul>
                      <li><a href="#">Auto Carousel</a></li>
                      <li><a href="#">Carousel Control</a></li>
                      <li><a href="#">Left & Right Navigation</a></li>
                      <li><a href="#">Four Columns Grid</a></li>
                    </ul>
                  </div>
                </li>
                <li><a href="#">Home & Garden</a></li>
                <li><a href="#">Jobs</a></li>
                <li><a href="#">Real Estate</a></li>
                <li><a href="#">Service for Hire</a></li>
                <li><a href="#">Volume My Cars</a></li>
                <li><a href="#">More...</a></li>
              </ul>
            </div>  
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
	<!--/ End Header -->
