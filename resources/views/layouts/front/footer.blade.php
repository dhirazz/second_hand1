<footer>
  <div class="top-footer">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
          <h4><i class="fa fa-car"></i>Vehicles <span>1076</span></h4>
          <ul>
            <li><a href="#">Cars</a></li>
            <li><a href="#">Bikes And Scooter</a></li>
            <li><a href="#">Cars Parts</a></li>
            <li><a href="#">Bikes And Scooter Accessories</a></li>
            <li><a href="#">Cars Accessories</a></li>
            <li><a href="#">Bycycles</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
          <h4><i class="fa fa-television"></i>Consumers Electronics <span>1076</span></h4>
          <ul>
            <li><a href="#">Home Theatre</a></li>
            <li><a href="#">television (LED, LCD,Plasma TV)</a></li>
            <li><a href="#">Cars Parts</a></li>
            <li><a href="#">Bikes And Scooter Accessories</a></li>
            <li><a href="#">Cars Accessories</a></li>
            <li><a href="#">Bycycles</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
          <h4><i class="fa fa-heart"></i>Health & Beauty <span>1076</span></h4>
          <ul>
            <li><a href="#">Fitness Equipment</a></li>
            <li><a href="#">Cosmetics</a></li>
            <li><a href="#">Personal Care</a></li>
            <li><a href="#">Perfumes</a></li>
            <li><a href="#">Health Equipments</a></li>
            <li><a href="#">Hair Care</a></li>
          </ul>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
          <h4><i class="fa fa-car"></i>Apparels & Accessories <span>1076</span></h4>
          <ul>
            <li><a href="#">Bags</a></li>
            <li><a href="#">Children';s Apparels</a></li>
            <li><a href="#">Jwellery</a></li>
            <li><a href="#">Men Apparels</a></li>
            <li><a href="#">Women Apparels</a></li>
            <li><a href="#">Shoes</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="bottom-footer">
    <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <p>© 2020 | All Right Reserved.</p>
        </div>
      </div>
    </div>
  </div>
</footer>