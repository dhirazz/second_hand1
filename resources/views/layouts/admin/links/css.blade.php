<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Oculux Bootstrap 4x admin is super flexible, powerful, clean &amp; modern responsive admin dashboard with unlimited possibilities.">
<meta name="keywords" content="admin template, Oculux admin template, dashboard template, flat admin template, responsive admin template, web app, Light Dark version">
<meta name="author" content="GetBootstrap, design by: puffintheme.com">
<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{asset('public/superadmin/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('public/superadmin/css/font-awesome/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('public/superadmin/css/vivify.min.css')}}">
<link rel="stylesheet" href="{{asset('public/superadmin/css/c3.min.css')}}"/>
<!-- MAIN CSS -->

<!-- datepicker -->

<link rel="stylesheet" href="{{asset('public/bootstrap-datepicker/bootstrap-datepicker3.css')}}">
<link rel="stylesheet" href="{{asset('public/superadmin/css/dropify/css/dropify.min.css')}}">
<link rel="stylesheet" href="{{asset('public/superadmin/css/site.min.css')}}">


<!-- dropdown search -->

<link href="{{asset('public/css/select2.css')}}" rel="stylesheet" />
<link href="{{asset('public/css/change.css')}}" rel="stylesheet" />


