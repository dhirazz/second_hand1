<nav id="left-sidebar-nav" class="sidebar-nav">
    <ul id="main-menu" class="metismenu">
        <li class="header">Main</li>

        <li class="@if( (request()->is('/admin')) ||
                            (request()->is('home'))
                        )
                        active
                        @endif">
            <a href="{{route('admin')}}"><i class="icon-speedometer"></i><span>Dashboard</span></a>
        </li>

        {{-- Main category --}}

        @if(in_array('maincategory.index',session('permission')))


        <li class="@if( (request()->is('/')) ||
                            (request()->is('category/category'))
                        )
                        active
                        @endif">
            <a href="{{route('maincategory.index')}}"><i class="icon-speedometer"></i><span>Main Category</span></a>
        </li>

    @endif

        @if(in_array('category.index',session('permission')))


            <li class="@if( (request()->is('/')) ||
                                (request()->is('category/category'))
                            )
                            active
                            @endif">
                <a href="{{route('category.index')}}"><i class="icon-speedometer"></i><span>Categories</span></a>
            </li>

        @endif

        {{-- Sub categories --}}

        @if(in_array('subcategory.index',session('permission')))


            <li class="@if( (request()->is('/')) ||
                                (request()->is('subcategory/subcategory'))
                            )
                            active
                            @endif">
                <a href="{{route('subcategory.index')}}"><i class="icon-speedometer"></i><span>SubCategories</span></a>
            </li>

        @endif

            {{-- Products --}}
            @if(in_array('product.index',session('permission')))

                <li class="@if( (request()->is('/')) ||
                    (request()->is('subcategory/subcategories'))
                )
                active
                @endif">
                    <a href="{{route('product.index')}}"><i class="icon-speedometer"></i><span>Product</span></a>
                </li>
            @endif

        {{-- Meta tag --}}
            @if(in_array('seometatag.index',session('permission')))

                <li class="@if( (request()->is('/')) ||
                    (request()->is('subcategory/subcategories'))
                )
                active
                @endif">
                    <a href="{{route('seometatag.index')}}"><i class="icon-speedometer"></i><span>SEO Meta</span></a>
                </li>
            @endif


        {{-- Roles --}}
        @if(in_array('role.index',session('permission')))

            <li class="@if( (request()->is('/')) ||
                (request()->is('role/subcategories'))
            )
            active
            @endif">
                <a href="{{route('role.index')}}"><i class="icon-speedometer"></i><span>Roles</span></a>
            </li>

        @endif

        {{-- Permission --}}

        @if(in_array('permission.index',session('permission')))


        <li class="@if( (request()->is('/')) ||
            (request()->is('subcategory/subcategories'))
        )
        active
        @endif">
            <a href="{{route('permission.index')}}"><i class="icon-speedometer"></i><span>Permissions</span></a>
        </li>

        @endif

        {{-- User management --}}


        @if(in_array('user.index',session('permission')))

            <li class="header">User Management</li>



            <li class="@if( (request()->is('user/users')) ||
                                (request()->is('user/edit-user/*'))
                            )
                            active
                            @endif">
                <a href="{{route('user.index')}}"><i class="icon-user"></i><span>Users</span></a>
            </li>
        @endif



        @if(in_array('user.index',session('permission')))

            <li class="@if( (request()->is('user/users')) ||
                                (request()->is('user/edit-user/*'))
                            )
                            active
                            @endif">
                <a href="{{route('customer.index')}}"><i class="icon-user"></i><span>Customers</span></a>
            </li>
        @endif

        {{-- Newsletter Subscriber --}}

        @if(in_array('newsletter.index',session('permission')))




        <li class="@if( (request()->is('user/users')) ||
                            (request()->is('user/edit-user/*'))
                        )
                        active
                        @endif">

            <a href="{{route('newsletter.index')}}">
                <i class="icon-user"></i><span>Newsletter Subscribers</span>
            </a>
        </li>
    @endif



        {{-- Contacts --}}

        @if(in_array('contact.index',session('permission')))




        <li class="@if( (request()->is('user/users')) ||
                            (request()->is('user/edit-user/*'))
                        )
                        active
                        @endif">

            <a href="{{route('contact.index')}}">
                <i class="icon-user"></i><span>Contacts</span>
            </a>
        </li>
    @endif

        {{-- Settings --}}


        @if(in_array('admin.settings',session('permission')))


            <li class="header">Settings</li>

            <li class=" @if( (request()->is('admin/settings')) )
                                    active
            @endif"><a href="{{route('admin.settings')}}"> <i class="icon-user"></i>Settings</a>
            </li>

        @endif



    </ul>
</nav>
