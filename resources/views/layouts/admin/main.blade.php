<!doctype html>
<html lang="en">
<head>

@php 
    $setting = \App\Setting::find(1);
 @endphp
<title> {{$setting->company_name}} | @yield('title')</title>

{{-- include the links --}}

@include('layouts.admin.links.css')



</head>
<body class="theme-cyan font-montserrat light_version">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>
<!-- Theme Setting -->
<!--  -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<div id="wrapper">
    <nav class="navbar top-navbar">
        <div class="container-fluid">

            <div class="navbar-left">
                <div class="navbar-btn">
                    <a href="#">
                        
                    <img src="{{asset('public/uploads/'.$setting->company_logo)}}"
                     alt="Logo" class="img-fluid logo"></a>
                    <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                </div>
                <!--  -->
            </div>
            
            <div class="navbar-right">

                <div id="navbar-menu">

                    <ul class="nav navbar-nav">
                        <li><a href="javascript:void(0);" class="search_toggle icon-menu" title="Search Result"><i class="icon-magnifier"></i></a></li>
                        <li><a href="javascript:void(0);" class="right_toggle icon-menu" title="Right Menu"><i class="icon-bubbles"></i><span class="notification-dot bg-pink">2</span></a></li>
                        
                        @include('layouts.admin.user_info')

                       
                        <li><a href="#" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();" class="icon-menu"><i class="icon-power"></i></a></li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </ul>
                </div>



            </div>
        </div>
        <div class="progress-container"><div class="progress-bar" id="myBar"></div></div>
    </nav>
    <div class="search_div">
        <div class="card">
            <div class="body">
                <form id="navbar-search" class="navbar-form search-form">
                    <div class="input-group mb-0">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="icon-magnifier"></i></span>
                            <a href="javascript:void(0);" class="search_toggle btn btn-danger"><i class="icon-close"></i></a>
                        </div>
                    </div>
                </form>
            </div>            
        </div>

        <!-- search result -->
        @include('layouts.admin.search-result')
    </div>

    <!-- mega-menu -->
    @include('layouts.admin.mega-menu')
    <!-- chat list tab -->

    @include('layouts.admin.chat_list')
    <div id="left-sidebar" class="sidebar">
        <div class="navbar-brand">
            <a href="#"><img src="{{asset('public/uploads/'.$setting->company_logo)}}" alt="Logo" class="img-fluid logo">
            <span> {{$setting->company_name}}</span></a>
            <button type="button" class="btn-toggle-offcanvas btn btn-sm float-right"><i class="lnr lnr-menu icon-close"></i></button>
        </div>
        <div class="sidebar-scroll">
            <!-- welcome , username section -->
             <!--if user is admin then provide him all navlinks  -->
            @include('layouts.admin.navbar')
        
        </div>
    </div>
     {{--  display index page --}}

     @yield('content')
</div>
<!-- Javascript -->

@include('layouts.admin.links.js')



</body>
</html>
