@php 
    $user = auth()->user();

    $admin = \Modules\Profile\Entities\AdminProfile::where('user_id',$user->id)->first();
    $photo = $admin->photo;

    $user = $user->name;
@endphp
<div class="user-account" style="display: inline;">
                <div class="user_div">
                        @if($photo)
                        <img src="{{asset('public/uploads/'.$photo)}}"  class="user-photo" style="object-fit: contain" alt="Display Picture">
                        @else
                            <img src="{{asset('public/uploads/avatar.webp')}}" class="user-photo" alt="Display  Picture">
                        @endif
                </div>
                <div class="dropdown">
                    <span>Welcome,</span>
                    <a href="javascript:void(0);" class="dropdown-toggle user-name" data-toggle="dropdown">
                        <strong>{{$user}}</strong>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right account vivify flipInY">

                        {{-- User profile --}}

                        @if((in_array('profile.edit',session('permission')) ||
                        in_array('profile.update',session('permission')))
                        
                        && in_array('profile.index',session('permission')) )
                            <li><a href="{{route('profile.index')}}"><i class="icon-user"></i>My Profile</a></li>
                        @endif


                  

                        {{-- change password --}}
                        
                        @if((in_array('change-password.index',session('permission'))  && 
                          in_array('change-password.update',session('permission'))) )

                            <li><a href="{{route('change-password.index')}}">
                                <i class="icon-user"></i>Change Password</a></li>
                        @endif

                        <li>
                            <a href="#" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();"
                             class="icon-menu"> <i class="icon-power"></i>Logout</a> </li>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <li class="divider"></li>

                    </ul>
                </div>                
            </div> 