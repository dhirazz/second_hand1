





(function($){
    $('.flash-item').owlCarousel({
        items: 3,
        loop:true,
        margin:10,        
        nav: true,
        dots: true,
        navText: ['<span class="fa fa-angle-left"></span>','<span class="fa fa-angle-right"></span>'],        
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
    
            600:{
                items:2
            },

            1000:{
                items:3
            }
        }
    });

})(jQuery);




//MOBILE MENU 

function openNav() {
  document.getElementById("mySidenav").style.width = "300px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}












