

/**
 *                              NEWSLETTER SUBSCRIBERS
 */
// on keyup submit the function

$('#newsletter_keyword').bind('keyup', function () {
    $('#newsletterform').delay(200).submit();
});

$(function () {
    $('#newsletterform').submit(function (e) {
        var route = $('#newsletterform').data('route');

        $('#table-subscriber > tbody').html('Loading..');

        // console.log("route",route);
        var form_data = $(this);

        var keyword = $('#newsletter_keyword').val();

        e.preventDefault();
        $.ajax({
            type: "GET",
            url: route,
            data: form_data.serialize(),



            success: function (data) {

                // if(!data){
                //     console.log('no data available');
                // }
                console.log(data);

                $('#table-subscriber > tbody').empty();



                jQuery.each(data, function (val) {

                    $('#table-subscriber > tbody').append(`<tr>
                 <td>${count++}</td>
                 <td>
                    ${this.name}
                    </td>
                    <td>
                    ${this.email}
                    </td>

                    <td>
                    ${this.mobile}
                    </td>

                    <td>
                    ${this.address}
                    </td>


                    <td>

                    <a  href='${window.location.href}/${this.id}/edit'>
                        <i class='icon-pencil'></i>
                    </a>

                    </td>

                    </tr>`);
                });
            }
        });
    });
});

/*
    MAIN CATEGORIES
*/

$('.main-category-keyword').bind('keyup', function () {
    $('#main-category-form').delay(200).submit();
});

$(function () {
    $('#main-category-form').submit(function (e) {
        var route = $('#main-category-form').data('route');

        console.log("Route: ", route);

        $('#table-maincategory > tbody').html('Loading..');

        // console.log("route",route);
        var form_data = $(this);

        // var keyword = $('#main-category-keyword').val();

        // console.log('Keyword',keyword);

        e.preventDefault();
        $.ajax({
            type: "GET",
            url: route,
            data: form_data.serialize(),
            success: function (data) {

                console.log(data);

                $('#table-maincategory > tbody').empty();



                jQuery.each(data, function (val) {




                    $('#table-maincategory > tbody').append(`<tr>
                    <td>


                    <label class='fancy-checkbox'>
                                                            <input class='checkbox-tick checkitem' type='checkbox' name='slug[]' value='${this.slug}'>
                                                            <span></span>
                                                        </label></td>
                    <td>


                        ${this.main_category}
                    </td>

                    <td>
                        <a  href='${window.location.href}/${this.slug}/edit'>
                            <i class='icon-pencil'></i>
                        </a>
                    </td>
                </tr>`);
                });
            }
        });
    });
});

/**
 *                              CATEGORIES
 */

$('.category-keyword').bind('keyup', function () {
    $('#category-form').delay(200).submit();
});



$(".date_from_category").datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
    onSelect: function () {
        $('#category-form').delay(200).submit();

    }
});



$(function () {
    $('#category-form').submit(function (e) {
        var route = $('#category-form').data('route');

        $('#table-category > tbody').html('Loading..');

        // console.log("route",route);
        var form_data = $(this);

        var keyword = $('#category-keyword').val();

        // console.log('Keyword',keyword);

        e.preventDefault();
        $.ajax({
            type: "GET",
            url: route,
            data: form_data.serialize(),
            success: function (data) {

                console.log(data);

                $('#table-category > tbody').empty();



                jQuery.each(data, function (val) {

                    $('#table-category > tbody').append(`<tr>
                    <td>
                        <label class='fancy-checkbox'>
                            <input class='checkbox-tick checkitem' type='checkbox' name='slug[]' value='${this.slug}'>
                            <span></span>
                        </label>
                    <td>
                        ${this.category_name}
                    </td>

                    <td>
                        <a  href='${window.location.href}/${this.slug}/edit'>
                            <i class='icon-pencil'></i>
                        </a>
                    </td>
                </tr>`);
                });
            }
        });
    });
});

/**
 *                           SUB CATEGORIES
 */

$('#subcategory-keyword').bind('keyup', function () {
    $('#subcategory-form').delay(200).submit();
});

$('#category_id').bind('change', function () {
    $('#subcategory-form').delay(200).submit();
});

$('#main_category_id').bind('change', function () {
    $('#subcategory-form').delay(200).submit();
});



$(function () {
    $('#subcategory-form').submit(function (e) {
        var route = $('#subcategory-form').data('route');

        $('#table-subcategory > tbody').html('Loading..');

        // console.log("route",route);
        var form_data = $(this);


        e.preventDefault();
        $.ajax({
            type: "GET",
            url: route,
            data: form_data.serialize(),
            success: function (data) {

                console.log(data);

                $('#table-subcategory > tbody').empty();



                jQuery.each(data, function (val) {

                    $('#table-subcategory > tbody').append(`<tr>
                    <td><label class='fancy-checkbox'>
                    <input class='checkbox-tick checkitem' type='checkbox' name='slug[]' value='${this.slug}'>
                    <span></span>
                </label></td>
                    <td>
                        ${this.subcategory_name}
                    </td>

                    <td>
                    ${this.category_name}
                </td>

                    <td>
                    ${this.main_category}
                </td>

                    <td>
                        <a  href='${window.location.href}/${this.slug}/edit'>
                            <i class='icon-pencil'></i>
                        </a>
                    </td>
                </tr>`);
                });
            }
        });
    });
});
