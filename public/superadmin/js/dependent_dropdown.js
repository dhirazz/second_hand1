// get category
jQuery(document).ready(function () {
    jQuery('select[name="main_category_id"]').on('change', function () {
       var categoryId = jQuery(this).val();
       $('select[name="category_id"]').empty();
       $('select[name="category_id"]').append($("<option value='loading' selected></option>").html('Loading...')).prop('disabled', true);
       if (categoryId) {
          var url = $('#category_url').val();
          jQuery.ajax({
             url: url + '/' + categoryId,
             type: "GET",
             dataType: "json",
             success: function (data) {
                console.log(data);
                jQuery('select[name="category_id"]').empty();
                $('select[name="category_id"]').append(`<option value="" selected>--Select Category--</option>`).prop('disabled', false);

                jQuery.each(data, function (val) {
                    $('select[name="category_id"]').append('<option value="' + this.id + '">' + this.category_name + '</option>');
                });
             }
          });
       }
       else {
          $('select[name="category_id"]').empty();
          $('select[name="category_id"]').append('<option value="" selected>--Select Category--</option>').prop('disabled', false);

       }
    });
 });

//  Get subcategory
jQuery(document).ready(function () {
    jQuery('select[name="category_id"]').on('change', function () {
       var categoryId = jQuery(this).val();
       $('select[name="subcategory_id"]').empty();
       $('select[name="subcategory_id"]').append($("<option value='loading' selected></option>").html('Loading...')).prop('disabled', true);
       if (categoryId) {
          var url = $('#subcategory_url').val();
          jQuery.ajax({
             url: url + '/' + categoryId,
             type: "GET",
             dataType: "json",
             success: function (data) {
                console.log(data);
                jQuery('select[name="subcategory_id"]').empty();
                $('select[name="subcategory_id"]').append('<option value="" selected>--Select SubCategory--</option>').prop('disabled', false);

                jQuery.each(data, function (val) {
                    $('select[name="subcategory_id"]').append('<option value="' + this.id + '">' + this.subcategory_name + '</option>');
                });
             }
          });
       }
       else {
          $('select[name="subcategory_id"]').empty();
          $('select[name="subcategory_id"]').append('<option value="" selected>--Select SubCategory--</option>').prop('disabled', false);

       }
    });
 });
