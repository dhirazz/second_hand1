
var count = 0;
function addElements(_class){
    var clone = $(_class).clone()
        .attr('id','row-'+count)  
        .end();   
    count++;
}




function removeElements(instance){



    var row = $('#expense-table').find('tbody tr:first').attr('id');

    var rowCount = $('#expense-table tbody tr').length;

    if(rowCount==1){
        console.log('Can not delete first row');
    }else{

        $(instance).closest('tr').remove()

    }

    calculateAmount();
    calculateDiscount();
    calculateFee();
    calculateTax();
}


function hideDelete(){
    var rowCount = $('#expense-table tbody tr').length;

    if(rowCount==1){
        $('.remove').hide();

    }else{
        $('.remove').show();

    }

    

}

hideDelete();