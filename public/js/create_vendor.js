$(function(){
    $('#form-data').submit(function(e){
        var route = $('#form-data').data('route');
        var form_data = $(this);

        $('select[name="vendor_id"]').prepend($("<option value='loading' selected></option>").html('Loading...')).css('background-color','#282B2F');
        $('select[name="campaign_id"]').prepend($("<option value='loading' selected></option>").html('Loading...')).css('background-color','#282B2F');


        e.preventDefault();
        $.ajax({
            type: "POST",
            url: route,
            data: form_data.serialize(),

            error: function(xhr, status, error) {
                var err = eval("(" + xhr.responseText + ")");
                console.log(error,err.Message);
              },
            success: function( data ) {

                var vendors = data['vendors'];
                var campaigns = data['campaigns'];

                console.log('Venodors',data['vendors']);
                console.log('Campaigns',data['campaigns']);

                jQuery('select[name="vendor_id"]').empty();

                jQuery.each(vendors, function(key,value){
                    $('select[name="vendor_id"]').append('<option selected value="'+ key +'">'+ value +'</option>');
                 });


                jQuery('select[name="campaign_id"]').empty();

                    $('select[name="campaign_id"]').append('<option selected value=>--Select Campaign--</option>');

                 jQuery.each(campaigns, function(key,value){
                    $('select[name="campaign_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                 });

                 $('#exampleModal').modal('hide');

                 $('.modal-backdrop').hide();
                 $('body').removeClass('modal-open');
                 return false;

            }
        });




    });
});



