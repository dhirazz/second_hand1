// $('.datepicker').datepicker({
//     format: 'mm/dd/yyyy',
//     todayHighlight:'TRUE',
//     autoclose: true,
// })
// .on('changeDate', function (ev) {
//     $(this).datepicker('hide');
// });

// $.fn.datepicker.defaults.autoclose = true;
$('*[data-provide="datepicker"]').datepicker({
        format: 'mm/dd/yyyy',
        todayHighlight:'TRUE',
        autoclose: true,
    })
    .on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
