-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 01, 2020 at 04:35 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gumtree`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_profile`
--

CREATE TABLE `admin_profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_profile`
--

INSERT INTO `admin_profile` (`id`, `user_id`, `photo`, `gender`, `dob`, `website`, `country`, `state`, `city`, `facebook`, `instagram`, `twitter`, `address`, `created_at`, `updated_at`) VALUES
(1, 2, '20200824071610-12.jpg', 'male', '1996-10-08', 'ojhasuren.wordpress.com', 'Nepal', 'Bagmati', 'Kathmadnu', 'facebook.com/GumTrees', 'instagram.com/GumTrees', 'twitter.com/GumTrees', 'Kirtipur , dadeldhura', '2020-08-24 01:23:27', '2020-08-24 01:33:50'),
(4, 8, '20200825014140-header.png', 'male', '1996-10-08', 'ojhasuren.wordpress.com', 'Norway', 'Bagmati', 'Kathmadnu', 'facebook.com/GumTrees', 'instagram.com/GumTrees', 'twitter.com/GumTrees', 'kirtipur , dadeldhura', '2020-08-24 19:56:40', '2020-08-24 19:56:40');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `main_category_id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `main_category_id`, `photo`, `category_name`, `slug`, `is_featured`, `created_at`, `updated_at`) VALUES
(10, 4, NULL, 'LCD', 'LCD', 0, '2020-09-29 16:42:47', '2020-09-29 16:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` int(11) NOT NULL DEFAULT 0,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `photo`, `main_category`, `featured`, `slug`, `created_at`, `updated_at`) VALUES
(4, NULL, 'Electronics', 0, '2009291026059392', '2020-09-29 16:41:05', '2020-09-29 16:41:05');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--
-- Error reading structure for table gumtree.menus: #1932 - Table 'gumtree.menus' doesn't exist in engine
-- Error reading data for table gumtree.menus: #1064 - You have an error in your SQL syntax; check the manual that corresponds to your MariaDB server version for the right syntax to use near 'FROM `gumtree`.`menus`' at line 1

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_20_071515_create_visitors_table', 2),
(5, '2020_08_20_111030_create_categories_table', 3),
(6, '2020_08_20_115403_create_sub_categories_table', 4),
(7, '2020_08_22_084251_create_s_e_o_meta_tags_table', 5),
(8, '2020_08_23_063801_create_permission_tables', 6),
(9, '2020_08_24_004149_create_admin_profile_table', 7),
(10, '2020_08_24_004159_create_user_profile_table', 7),
(11, '2020_08_25_104112_create_products_table', 8),
(12, '2020_08_26_004236_create_product_photos_table', 8),
(13, '2020_09_10_091459_create_news_letter_templates_table', 9),
(14, '2020_09_10_091521_create_news_letter_subscribers_table', 9),
(15, '2020_09_11_025708_create_contacts_table', 10),
(16, '2020_09_21_032902_create_my_ads_table', 11),
(17, '2020_09_25_010656_create_menus_table', 12),
(18, '2020_09_27_064853_create_main_categories_table', 13);

-- --------------------------------------------------------

--
-- Table structure for table `my_ads`
--

CREATE TABLE `my_ads` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `visitor_id` bigint(20) UNSIGNED NOT NULL,
  `ad_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news_letter_subscribers`
--

CREATE TABLE `news_letter_subscribers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_letter_subscribers`
--

INSERT INTO `news_letter_subscribers` (`id`, `name`, `email`, `mobile`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Surendra Ojha', 'sojha060@gmail.com', '9860347384', 'kathmandu', '2020-09-10 04:08:12', '2020-09-10 04:08:12'),
(2, 'Surendra Ojha', 'ram@gmail.com', '9860347386', 'kathmandu', '2020-09-10 04:35:19', '2020-09-10 04:35:19'),
(3, '', 'hari@gmail.com', NULL, NULL, '2020-09-19 19:39:13', '2020-09-19 19:39:13');

-- --------------------------------------------------------

--
-- Table structure for table `news_letter_templates`
--

CREATE TABLE `news_letter_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `templete` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'public',
  `permission_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `module`, `link`, `type`, `permission_code`, `created_at`, `updated_at`) VALUES
(262, 'change-password', 'change-password.store', 'create', '9fede65af6513ef223fb0526fb9869512008251235273492531', '2020-08-24 18:50:27', '2020-08-24 18:50:27'),
(263, 'change-password', 'change-password.index', 'view', '1b0d82dac519b0484836ebbf5cd423402008251235273547497', '2020-08-24 18:50:27', '2020-08-24 18:50:27'),
(264, 'change-password', 'change-password.edit', 'edit', '39af346f9bf97ad810ec8f3f6e7dfa132008251235273037567', '2020-08-24 18:50:27', '2020-08-24 18:50:27'),
(265, 'change-password', 'change-password.delete', 'delete', '8f4ca1fad46e1a72c59d2e25a09b3bb92008251235278013167', '2020-08-24 18:50:27', '2020-08-24 18:50:27'),
(266, 'seometatag', 'seometatag.store', 'create', '9a16b52e849f904bf4626f3fd9ee8da62008250124141976063', '2020-08-24 19:39:14', '2020-08-24 19:39:14'),
(267, 'seometatag', 'seometatag.index', 'view', 'cbf99ca0f2908a9fbe38b3d0fde729a62008250124143587538', '2020-08-24 19:39:14', '2020-08-24 19:39:14'),
(268, 'seometatag', 'seometatag.edit', 'edit', '9762f5a3b2d93ac181739e5054f0e59b200825012414471379', '2020-08-24 19:39:14', '2020-08-24 19:39:14'),
(269, 'seometatag', 'seometatag.delete', 'delete', 'ad66bca95b6f0380ded4c5fe20022c812008250124142360996', '2020-08-24 19:39:14', '2020-08-24 19:39:14'),
(270, 'category', 'category.store', 'create', 'ea4f160b0eb039cadd052dc7cce6753a2008250124205410083', '2020-08-24 19:39:20', '2020-08-24 19:39:20'),
(271, 'category', 'category.index', 'view', 'b0a4b6b5f4c82f883e9004544ba705772008250124206884427', '2020-08-24 19:39:20', '2020-08-24 19:39:20'),
(272, 'category', 'category.edit', 'edit', 'dffbafd9ae3e07f28ebff48668aa825d2008250124205083973', '2020-08-24 19:39:20', '2020-08-24 19:39:20'),
(273, 'category', 'category.delete', 'delete', '08ba7c91b6a847129889351b5a571e7b2008250124201210463', '2020-08-24 19:39:20', '2020-08-24 19:39:20'),
(274, 'subcategory', 'subcategory.store', 'create', '3e4d2ded7a738db2312a1df39b975b4b2008250124296534643', '2020-08-24 19:39:29', '2020-08-24 19:39:29'),
(275, 'subcategory', 'subcategory.index', 'view', '9021b8be20f05277b68cd0bedf9982572008250124292425192', '2020-08-24 19:39:29', '2020-08-24 19:39:29'),
(276, 'subcategory', 'subcategory.edit', 'edit', '54b70d30772bdeeb98dfc00009836d762008250124298464056', '2020-08-24 19:39:29', '2020-08-24 19:39:29'),
(277, 'subcategory', 'subcategory.delete', 'delete', 'a2ee92c0717efa4d9f70050717a4e57a2008250124294449345', '2020-08-24 19:39:29', '2020-08-24 19:39:29'),
(278, 'role', 'role.store', 'create', '92b5dafcbf78f4fb5732d78c5914c5582008250124388498328', '2020-08-24 19:39:38', '2020-08-24 19:39:38'),
(279, 'role', 'role.index', 'view', 'e60767af8273308db4919816ec614e422008250124388372674', '2020-08-24 19:39:38', '2020-08-24 19:39:38'),
(280, 'role', 'role.edit', 'edit', 'e51669b1f9b6a8f8150d56442548b2dc200825012438773029', '2020-08-24 19:39:38', '2020-08-24 19:39:38'),
(281, 'role', 'role.delete', 'delete', '7873f0fb26523ee3b3ba4054606b0212200825012438469999', '2020-08-24 19:39:38', '2020-08-24 19:39:38'),
(282, 'permission', 'permission.store', 'create', 'e93b39678320860d89f9da5d0621bc482008250124447325407', '2020-08-24 19:39:44', '2020-08-24 19:39:44'),
(283, 'permission', 'permission.index', 'view', 'fddac856e7f4caa3f5a4462a292a773c2008250124446552755', '2020-08-24 19:39:44', '2020-08-24 19:39:44'),
(284, 'permission', 'permission.edit', 'edit', 'ed32aa540419e0becc6fe0afd342b6422008250124448269000', '2020-08-24 19:39:44', '2020-08-24 19:39:44'),
(285, 'permission', 'permission.delete', 'delete', '3a785bf45772bc26aeb8c2ed08263fd72008250124447885000', '2020-08-24 19:39:44', '2020-08-24 19:39:44'),
(286, 'user', 'user.store', 'create', 'bac3381b95af0850a0c05ad3c06052d02008250125029314029', '2020-08-24 19:40:02', '2020-08-24 19:40:02'),
(287, 'user', 'user.index', 'view', 'de3cdb8f1268527b5373313cdb6e9c85200825012502901568', '2020-08-24 19:40:02', '2020-08-24 19:40:02'),
(288, 'user', 'user.edit', 'edit', '7cadf4b0cae4e29a45014b4f58be39e92008250125024037822', '2020-08-24 19:40:02', '2020-08-24 19:40:02'),
(289, 'user', 'user.delete', 'delete', '9da6c1d51213f705dd69b5a09baafb962008250125027877763', '2020-08-24 19:40:02', '2020-08-24 19:40:02');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mississippi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `title`, `subcategory_id`, `price`, `condition`, `mississippi`, `contact_name`, `email`, `ad_number`, `website`, `country`, `state`, `city`, `location`, `video`, `slug`, `created_at`, `updated_at`) VALUES
(38, '', 'Dell', 12, '2500', 'Used', 'Mr', 'contact name', 'sojha060@gmail.com', '120', 'www.surendraojha.com', NULL, NULL, NULL, 'location,location', NULL, 'ab2724d10b490217916b1bcc56aef48f2009291115166222', '2020-09-29 17:30:16', '2020-09-29 17:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `product_photos`
--

CREATE TABLE `product_photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured_image` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'regular',
  `role_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `type`, `role_code`, `created_at`, `updated_at`) VALUES
(7, 'superadmin', 'superadmin', '', NULL, NULL),
(11, 'admin', 'regular', '21232f297a57a5a743894a0e4a801fc32008251236118377247', '2020-08-24 18:51:11', '2020-08-24 18:51:11'),
(12, 'settings', 'regular', '2e5d8aa3dfa8ef34ca5131d20f9dad512009140306417797294', '2020-09-14 09:21:41', '2020-09-14 09:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `permission_id`, `status`, `created_at`, `updated_at`) VALUES
(3297, 11, 263, '0', '2020-08-24 18:56:27', '2020-08-24 18:56:27'),
(3298, 11, 264, '0', '2020-08-24 18:56:27', '2020-08-24 18:56:27'),
(3299, 12, 270, '0', '2020-09-14 09:21:41', '2020-09-14 09:21:41'),
(3300, 12, 271, '0', '2020-09-14 09:21:41', '2020-09-14 09:21:41'),
(3301, 12, 273, '0', '2020-09-14 09:21:41', '2020-09-14 09:21:41'),
(3302, 12, 263, '0', '2020-09-14 09:21:41', '2020-09-14 09:21:41'),
(3303, 12, 283, '0', '2020-09-14 09:21:41', '2020-09-14 09:21:41');

-- --------------------------------------------------------

--
-- Table structure for table `seo_meta_tags`
--

CREATE TABLE `seo_meta_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_keyword` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `auth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seo_meta_tags`
--

INSERT INTO `seo_meta_tags` (`id`, `name`, `title`, `meta_desc`, `meta_keyword`, `auth`, `slug`, `created_at`, `updated_at`) VALUES
(2, 'Surendra Ojha', 'Create User', 'surendra description', 'surendra description', 'Surendra oJha', 'Create User', '2020-08-22 07:25:24', '2020-08-22 07:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT 'english',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `company_name`, `email`, `phone`, `company_logo`, `address`, `twitter`, `facebook`, `instagram`, `date_type`, `created_at`, `updated_at`) VALUES
(1, 'GumTrees', 'gumTrees@yamail.com', '9860347384', '20200822045012-info.jpg', 'Amargadhi , Dadeldhura', 'twitter.com/GumTrees', 'facebook.com/GumTrees', 'instagram.com/GumTrees', 'English', '2020-04-08 05:46:02', '2020-08-22 11:05:12');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `subcategory_name`, `photo`, `slug`, `created_at`, `updated_at`) VALUES
(12, 10, 'Samsung', NULL, 'Samsung', '2020-09-29 16:49:07', '2020-09-29 16:49:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `mobile`, `username`, `email_verified_at`, `password`, `profile_code`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 7, 'Surendra Ojha', 'sojha060@gmail.com', '9860347384', 'sojha060', NULL, '$2y$10$fKMt5ciV0meww0RJQyVF2en.dv1atBfM2LRQjpwd/D3E413J7dhzC', '16e7306ed3e16906d7e23730bfb22cd9200824084403', NULL, NULL, '2020-08-24 18:32:48'),
(8, 11, 'Ram Ojha', 'ram060@gmail.com', '9960347389', 'ramram', NULL, '$2y$10$8i5vQ192VWc9Yjgxyh036uI5C.WZ7BfRVgi3vBFGSfkFWqKlqmX82', '7f1be6514c4994d7da8d128b8a6be6fe200825013807', NULL, '2020-08-24 19:53:07', '2020-08-24 19:53:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` date NOT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `photo`, `gender`, `dob`, `website`, `country`, `state`, `city`, `facebook`, `instagram`, `twitter`, `address`, `created_at`, `updated_at`) VALUES
(1, 1, '20200919064939-119181352_333637474542976_5722242722196503294_n.jpg', 'male', '2020-09-22', 'ojhasuren.wordpress.com', 'Nepal', 'Bagmati', 'kathmandu', 'facebook.com/GumTrees', 'instagram.com/GumTrees', 'twitter.com/GumTrees', 'kathmandu', '2020-09-19 01:04:08', '2020-09-19 01:09:28');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `name`, `email`, `mobile`, `username`, `email_verified_at`, `password`, `remember_token`, `provider_id`, `profile_code`, `created_at`, `updated_at`) VALUES
(1, 'Surendra Ojha', 'sojha060@gmail.com', '9860347384', 'sojha0600', NULL, '$2y$10$Sa3p4PELNOeBkv/a87g0x.iElEcQ8JvKJrdksmcm.nuaYWSTxess.', NULL, NULL, NULL, '2020-08-20 03:08:50', '2020-09-19 01:12:24'),
(8, 'Hari', 'hari@gmail.com', '9860347380', 'hario1234', NULL, '$2y$10$CObyXmz7JSqcMVJXxHiHne.OuWorbN8Lua9ld7tjUos.SEXwW8Nxa', NULL, NULL, NULL, '2020-09-19 23:01:50', '2020-09-19 23:25:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_profile`
--
ALTER TABLE `admin_profile`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_profile_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `foreign_main_category_id` (`main_category_id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_ads`
--
ALTER TABLE `my_ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `my_ads_visitor_id_foreign` (`visitor_id`),
  ADD KEY `my_ads_ad_id_foreign` (`ad_id`);

--
-- Indexes for table `news_letter_subscribers`
--
ALTER TABLE `news_letter_subscribers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_letter_templates`
--
ALTER TABLE `news_letter_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_subcategory_id_foreign` (`subcategory_id`);

--
-- Indexes for table `product_photos`
--
ALTER TABLE `product_photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_photos_product_id_foreign` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_role_permission_role_id` (`role_id`),
  ADD KEY `fk_role_permission_permission_id` (`permission_id`);

--
-- Indexes for table `seo_meta_tags`
--
ALTER TABLE `seo_meta_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_category_id_foreign` (`category_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `foreign_fk_user_role_id` (`role_id`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_profile_user_id_foreign` (`user_id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `visitors_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_profile`
--
ALTER TABLE `admin_profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `my_ads`
--
ALTER TABLE `my_ads`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news_letter_subscribers`
--
ALTER TABLE `news_letter_subscribers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `news_letter_templates`
--
ALTER TABLE `news_letter_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=290;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `product_photos`
--
ALTER TABLE `product_photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3304;

--
-- AUTO_INCREMENT for table `seo_meta_tags`
--
ALTER TABLE `seo_meta_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_profile`
--
ALTER TABLE `admin_profile`
  ADD CONSTRAINT `admin_profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `foreign_main_category_id` FOREIGN KEY (`main_category_id`) REFERENCES `main_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `my_ads`
--
ALTER TABLE `my_ads`
  ADD CONSTRAINT `my_ads_ad_id_foreign` FOREIGN KEY (`ad_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `my_ads_visitor_id_foreign` FOREIGN KEY (`visitor_id`) REFERENCES `visitors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_photos`
--
ALTER TABLE `product_photos`
  ADD CONSTRAINT `product_photos_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD CONSTRAINT `foreign_permission_fk` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `foreign_roles_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `foreign_fk_user_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD CONSTRAINT `user_profile_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `visitors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
