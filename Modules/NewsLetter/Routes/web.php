<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'admin','middleware'=>['auth','rbac']],function() {
    Route::resource('newsletter', 'NewsLetterController');
    Route::post('delete','NewsLetterController@delete')->name('newsletter.delete');

    Route::get('search','NewsLetterController@search')->name('newsletter.search');

});

Route::post('subscribe','NewsLetterFrontController@store')->name('newsletter.subscribe');
