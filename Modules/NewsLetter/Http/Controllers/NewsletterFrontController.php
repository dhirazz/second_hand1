<?php

namespace Modules\NewsLetter\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\NewsLetter\Entities\NewsLetterSubscriber;

class NewsletterFrontController extends Controller
{

    public function store(Request $request)
    {
        $rules = [
            'email' => 'required|email|unique:news_letter_subscribers,email'
        ];

        $request->validate($rules);

        $newsletter = new NewsLetterSubscriber();
        $newsletter->email = $request->email;
        $newsletter->save();

        return back()->with('msg','Thank You For Subscribing Our Newsletter!!');
    }


}
