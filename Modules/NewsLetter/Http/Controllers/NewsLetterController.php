<?php

namespace Modules\NewsLetter\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\NewsLetter\Entities\NewsLetterSubscriber;
use PhpParser\Node\Stmt\Foreach_;

class NewsLetterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $newsletter_subscribers = NewsLetterSubscriber::orderBy('email')->get();

        return view('newsletter::admin.index',
            compact('newsletter_subscribers')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('newsletter::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'name'=>'required',
            'email'=>'required|unique:news_letter_subscribers',
            'mobile'=>'required|unique:news_letter_subscribers',
            'address'=>'required'
        ];

        $request->validate($rules);
        $newsletter = new NewsLetterSubscriber();
        $newsletter->name = $request->name;
        $newsletter->email = $request->email;
        $newsletter->mobile = $request->mobile;
        $newsletter->address = $request->address;
        $newsletter->save();

        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('newsletter::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {

        $newsletter = NewsLetterSubscriber::find($id);
        return view('newsletter::admin.edit',
            compact('newsletter')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'=>'required',
            'email'=>'required|unique:news_letter_subscribers,email,'.$id,
            'mobile'=>'required|unique:news_letter_subscribers,mobile,'.$id,
            'address'=>'required'
        ];

        $request->validate($rules);

        $newsletter = NewsLetterSubscriber::find($id);
        $newsletter->name = $request->name;
        $newsletter->email = $request->email;
        $newsletter->mobile = $request->mobile;
        $newsletter->address = $request->address;
        $newsletter->save();

        return redirect()->route('newsletter.index');
    }


    // Delete

    public function delete(Request $request){

        $id = $request->id;

        foreach ($id as $value) {
            $newsletter =NewsLetterSubscriber::find($id);
            $newsletter->delete();
        }

        return back();

    }


    // function for ajax search

    public function search(Request $request){
        $keyword = $request->keyword;
        $newsletter_subscribers = NewsLetterSubscriber::where(function($q) use ($keyword){
            $q->where('email', 'like', '%' .$keyword . '%')
              ->orWhere('mobile', 'like', '%' .$keyword. '%')
              ->orWhere('name', 'like', '%' .$keyword. '%')
              ->orWhere('address', 'like', '%' .$keyword. '%');

        })->get();

        return response()->json($newsletter_subscribers);
    }
}
