@extends('layouts.admin.main')

@section('title')
Edit User
@endsection

@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Edit User</h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#addUser">Edit
                                User</a></li>
                    </ul>
                    <div class="tab-content mt-0">

                        <div class="tab-pane active show" id="addUser">
                            <div class="body mt-2">
                                
                                <form action="{{route('newsletter.update',$newsletter->id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="row clearfix">
                                            {{-- Name --}}

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                       

                                                    <label for="name" 
                                                    class="input-group-text">Full Name</label>
                                                    </div>
                                                    <input type="text" name="name"
                                                     id="name"
                                                    class="form-control show-tick @error('name') is-invalid @enderror"
                                                    placeholder="Full Name *" value="{{old('name',$newsletter->name)}}"
                                                    required="">
                                                </div>
                                            </div>
                                        </div>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror



                                        {{-- Email --}}

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                       

                                                    <label for="email" 
                                                    class="input-group-text">Email</label>
                                                    </div>
                                                    <input type="email" name="email"
                                                     id="email"
                                                    class="form-control show-tick @error('email') is-invalid @enderror"
                                                    placeholder="Email *" value="{{old('email',$newsletter->email)}}"
                                                    required="">
                                                </div>
                                            </div>
                                        </div>


                                        

                                        {{-- Mobile --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <label for="mobile" 
                                                    class="input-group-text">Mobile</label>
                                                    </div>
                                                    <input type="text" name="mobile"
                                                     id="mobile"
                                                    class="form-control show-tick @error('mobile') is-invalid @enderror"
                                                    placeholder="Mobile *" value="{{old('mobile',$newsletter->mobile)}}">
                                                </div>
                                            </div>
                                        </div>


                                        {{-- Address --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                       

                                                    <label for="address" 
                                                    class="input-group-text">Address</label>
                                                    </div>
                                                    <input type="text" name="address"
                                                     id="address"
                                                    class="form-control show-tick @error('address') is-invalid @enderror"
                                                    placeholder="Address *" value="{{old('address',$newsletter->address)}}">
                                                </div>
                                            </div>
                                        </div>
                                    


                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <a class="btn btn-secondary"
                                                href="{{ route('newsletter.index') }}">
                                                CLOSE
                                            </a>
                                        </div>

                                    </div>

                                    @if($errors->any())
                                    {!! implode('', $errors->all("<div class='error'>
                                        <p style='color:red'>:message</p>
                                    </div>")) !!}
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
