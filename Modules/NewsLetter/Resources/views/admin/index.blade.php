@extends('layouts.admin.main')




@section('title')
    Newsletter Subscribers
@endsection
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>
                        Newsletter Subscribers
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">


            {{-- Search form --}}

            @include('newsletter::admin.search')

            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#Users"> Newsletter Subscribers
                            </a>
                        </li>
                        @if(in_array('category.store',session('permission')))

                            <li class="nav-item">
                                <a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Add New Subscriber</a>
                            </li>
                        @endif
                    </ul>

                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">
                            <div class="table-responsive">

                                <form method="post">
                                    @csrf
                                    @if(in_array('newsletter.delete',session('permission')))

                                    <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('newsletter.delete') }}" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>

                                    @endif
                                    <table class="table table-hover table-custom spacing8"
                                    id="table-subscriber"
                                    >
                                        <thead>
                                            <tr>
                                                @if(in_array('category.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick"
                                                         type="checkbox" id="check_all">
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                                @endif


                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Mobile</th>
                                                <th>Address</th>

                                                @if(in_array('category.edit',session('permission')))

                                                    <th>Actions</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($newsletter_subscribers as $value)

                                            <tr>
                                                @if(in_array('newsletter.delete',session('permission')))

                                                    <td>

                                                        <label class="fancy-checkbox">
                                                            <input class="checkbox-tick checkitem" type="checkbox"
                                                                name="id[]" value="{{ $value->id }}">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                @endif

                                                <td>
                                                     {{$value->name}}                                          </td>

                                                <td>
                                                     {{$value->email}}                                              </td>
                                                <td>
                                                     {{$value->mobile}}                                                </td>

                                                <td>
                                                     {{$value->address}}
                                                </td>

                                                @if(in_array('newsletter.edit',session('permission')))


                                                    <td>
                                                        <a href="{{route('newsletter.edit',$value->id)}}">
                                                            <i class="icon-pencil"></i>
                                                        </a>

                                                    </td>

                                                @endif

                                                @endforeach
                                        </tbody>
                                    </table>
                                </form>
                                {{-- {!! $Newsletter Subscribers->render() !!} --}}

                            </div>
                        </div>

                        @if(in_array('newsletter.store',session('permission')))

                        <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                            <div class="body mt-2">

                                {{-- @include('expensecategory::common.create') --}}

                                <form action="{{route('newsletter.store')}}" method="post">
                                    @csrf
                                    <div class="row clearfix">
                                            {{-- Name --}}

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">


                                                    <label for="name"
                                                    class="input-group-text">Full Name</label>
                                                    </div>
                                                    <input type="text" name="name"
                                                     id="name"
                                                    class="form-control show-tick @error('name') is-invalid @enderror"
                                                    placeholder="Full Name *" value="{{old('name')}}"
                                                    required="">
                                                </div>
                                            </div>
                                        </div>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror



                                        {{-- Email --}}

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">


                                                    <label for="email"
                                                    class="input-group-text">Email</label>
                                                    </div>
                                                    <input type="email" name="email"
                                                     id="email"
                                                    class="form-control show-tick @error('email') is-invalid @enderror"
                                                    placeholder="Email *" value="{{old('email')}}"
                                                    required="">
                                                </div>
                                            </div>
                                        </div>




                                        {{-- Mobile --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                    <label for="mobile"
                                                    class="input-group-text">Mobile</label>
                                                    </div>
                                                    <input type="text" name="mobile"
                                                     id="mobile"
                                                    class="form-control show-tick @error('mobile') is-invalid @enderror"
                                                    placeholder="Mobile *" value="{{old('mobile')}}">
                                                </div>
                                            </div>
                                        </div>


                                        {{-- Address --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">


                                                    <label for="address"
                                                    class="input-group-text">Address</label>
                                                    </div>
                                                    <input type="text" name="address"
                                                     id="address"
                                                    class="form-control show-tick @error('address') is-invalid @enderror"
                                                    placeholder="Address *" value="{{old('address')}}">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                            <a class="btn btn-secondary"
                                                href="{{ route('newsletter.index') }}">
                                                CLOSE
                                            </a>
                                        </div>

                                    </div>

                                    @if($errors->any())
                                    {!! implode('', $errors->all("<div class='error'>
                                        <p style='color:red'>:message</p>
                                    </div>")) !!}
                                    @endif
                                </form>
                            </div>
                        </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection
