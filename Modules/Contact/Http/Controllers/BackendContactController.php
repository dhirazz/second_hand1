<?php

namespace Modules\Contact\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Contact\Entities\Contact;

class BackendContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('contact::admin.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('contact::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',					 	 	
            'subject'	=> 'required',							 	 	
            'email'		=> 'required',						 	 	
            'phone'		=> 'required',						 	 	
            'message' => 'required'
        ];

        

        $request->validate($rules);
        $contact = new Contact();
        $contact->name = $request->name;					 	 	
		$contact->subject = $request->name;				 	 	
		$contact->email	= $request->name;	
		$contact->phone	= $request->name;
        $contact->message= $request->name;
        
        $contact->save();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('contact::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('contact::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required',					 	 	
            'subject'	=> 'required',							 	 	
            'email'		=> 'required',						 	 	
            'phone'		=> 'required',						 	 	
            'message' => 'required'
        ];

        

        $request->validate($rules);
        $contact = Contact::find($id);
        $contact->name = $request->name;					 	 	
		$contact->subject = $request->name;				 	 	
		$contact->email	= $request->name;	
		$contact->phone	= $request->name;
        $contact->message= $request->name;
        
        $contact->save();

        return redirect()->route('contact.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete(Request $request)
    {
        if($request->id){
            $contact = Contact::whereIn('id',$request->id)->delete();
        }

        return back();
    }
}
