@extends('layouts.admin.main')




@section('title')
Categories
@endsection
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Categories
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- search -->

            {{-- @include('bank::common.search') --}}
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#Users"> Categories
                            </a></li>
                            @if(in_array('category.store',session('permission')))

                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Add New Category
                            </a></li>
                            @endif
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">
                            <div class="table-responsive">

                                <form method="post">
                                    @csrf
                                    @if(in_array('category.delete',session('permission')))

                                    <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('category.delete') }}" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>

                                    @endif
                                    <table class="table table-hover table-custom spacing8">
                                        <thead>
                                            <tr>
                                                @if(in_array('category.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick" type="checkbox" id="check_all">
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                                @endif
                                                <th>Category</th>

                                                @if(in_array('category.edit',session('permission')))

                                                    <th>Actions</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($categories as $value)

                                            <tr>
                                                @if(in_array('category.delete',session('permission')))

                                                    <td>

                                                        <label class="fancy-checkbox">
                                                            <input class="checkbox-tick checkitem" type="checkbox"
                                                                name="slug[]" value="{{ $value->slug }}">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                @endif

                                                <td>
                                                    <h6 class="mb-0"> {{$value->category_name}}</h6>
                                                </td>

                                                @if(in_array('category.edit',session('permission')))


                                                    <td>
                                                        <a href="{{route('category.edit',$value->slug)}}">
                                                            <i class="icon-pencil"></i>
                                                        </a>

                                                    </td>

                                                @endif

                                                @endforeach
                                        </tbody>
                                    </table>
                                </form>
                                {{-- {!! $categories->render() !!} --}}

                            </div>
                        </div>

                        @if(in_array('category.store',session('permission')))

                        <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                            <div class="body mt-2">

                                {{-- @include('expensecategory::common.create') --}}

                                <form action="{{route('category.store')}}" enctype="multipart/form-data" method="post">
                                    @csrf
                                    <div class="row clearfix">

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                       

                                                        <label for="category_name" class="input-group-text">Category</label>
                                                    </div>
                                                    <input type="text" name="category_name" id="category_name"
                                                        class="form-control show-tick @error('category_name') is-invalid @enderror"
                                                        placeholder="Category *" value="{{old('category_name')}}"
                                                        required="">
                                                </div>
                                            </div>
                                        </div>

                                        @error('category_name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror


                                        {{-- is featured --}}

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox"
                                                        name="is_featured" value="1">
                                                    <span>Is Featured?</span>
                                                </label>

                                            </div>
                                        </div>


                                        {{-- photo upload --}}

                                        <div class="col-12">
                                            <div class="form-group mt-3 mb-5">
                                                <input type="file" name="photo" class="dropify">
                                                <small id="fileHelp" class="form-text text-muted">This is some
                                                    placeholder block-level help text for the above input. It's a bit
                                                    lighter and easily wraps to a new line.</small>
                                            </div>
                                        </div>


                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                            <a class="btn btn-secondary"
                                                href="{{url('expensecategory/expensecategories')}}">
                                                CLOSE
                                            </a>
                                        </div>

                                    </div>

                                    @if($errors->any())
                                    {!! implode('', $errors->all("<div class='error'>
                                        <p style='color:red'>:message</p>
                                    </div>")) !!}
                                    @endif
                                </form>



                            </div>
                        </div>

                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>




@endsection