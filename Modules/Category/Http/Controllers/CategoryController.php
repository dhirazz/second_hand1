<?php

namespace Modules\Category\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;

use Illuminate\Http\Request;

use Illuminate\Routing\Controller;

use Modules\Category\Entities\Category;

use File;
use Modules\MainCategory\Entities\MainCategory;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $categories = Category::orderBy('id','DESC')
                    ->paginate(config('number_of_pages'));
        $main_categories = MainCategory::orderBy('main_category')->get();

        // to set default in search

        $date_from = date('1-m-Y');
        $date_to = date('t-m-Y');
        return view('category::admin.index',
            compact('categories','date_from','date_to','main_categories')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('category::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {

        $rules = [
            'main_category_id'=>'required',
            'category_name'=>'required|unique:categories,category_name'
        ];
        $request->validate($rules);

        $category = new Category;
        $category->main_category_id = $request->main_category_id;
        $category->category_name = $request->category_name;
        $category->slug = $request->category_name;

        if($request->is_featured){
            $category->is_featured = $request->is_featured;
        }

        if($request->hasFile('photo')){
            $file = $request->file('photo');
            $photo = time().'.'.$file->getClientOriginalExtension();
            $destination_path = public_path('/uploads');
            $file->move($destination_path,$photo);

            $category->photo = $photo;
        }
        $category->save();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('category::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $category = Category::where('slug',$id)->first();
        $main_categories = MainCategory::orderBy('main_category')->get();
            return view('category::admin.edit',
            compact('category','main_categories')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request,$id)
    {
        //
        $category = Category::where('slug',$id)->first();

        $rules = [
            'category_name'=>'required|unique:categories,category_name,'.$category->id
        ];

        $request->validate($rules);

        $category->category_name = $request->category_name;

        $category->main_category_id = $request->main_category_id;

        $category->slug = $request->category_name;

        if($request->is_featured){
            $category->is_featured = $request->is_featured;
        }else{
            $category->is_featured = '0';

        }



        if($request->hasFile('photo')){

            //remove file from the folder

            $old_file = public_path('/uploads').'/'.$category->photo;

            if(File::exists($old_file)){
                File::delete($old_file);
            }

            //upload new one
            $file = $request->file('photo');
            $photo = time().'.'.$file->getClientOriginalExtension();
            $destination_path = public_path('/uploads');
            $file->move($destination_path,$photo);

            $category->photo = $photo;
        }

        $category->save();

        return redirect()->route('category.index');
    }

    // delete

    public function delete(Request $request)
    {

        if ($request->slug) {
            $subcategory = Category::whereIn('slug', $request->slug)->delete();
        }

        return back();
    }


    // Ajax function for filtering

    public function search(Request $request){
        $keyword = $request->keyword;
        $date_from = date('Y-m-d',strtotime($request->date_from)) ;
        $date_to = date('Y-m-d',strtotime($request->date_to));
        $categories = Category::where('category_name', 'like', '%' .$keyword . '%')
                        ->whereBetween('created_at', [$date_from, $date_to])
                        ->get();
        return response()->json($categories);

    }
}
