@extends('layouts.admin.main')
@section('title')
Edit Category
@endsection
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Edit Category</h2>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#addUser">
                                Edit Category</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane  active show" id="addUser">
                            <div class="body mt-2">
                                <form action="{{route('category.update',$category->slug)}}" method="post"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <input name="_method" type="hidden" value="PUT">

                                    <div class="row clearfix">

                                        {{-- Main category --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Main Category</span>
                                                    </div>
                                                    <select name="main_category_id" id="" class="form-control">
                                                        <option value="">--Select Main Category--</option>
                                                        @foreach ($main_categories as $value)
                                                            <option value="{{ $value->id }}"
                                                                {{ old('main_category_id',$category->main_category_id) == $value->id ? 'selected' : '' }}
                                                            >{{ $value->main_category }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        {{-- Category Name --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Category</span>
                                                    </div>
                                                    <input type="text" name="category_name" class="form-control"
                                                        placeholder="Category Name *"
                                                        value="{{old('category_name',$category->category_name)}}"
                                                        required="">
                                                </div>
                                            </div>
                                        </div>

                                          {{-- is featured --}}

                                          <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox"
                                                        name="is_featured"
                                                         @if($category->is_featured==1) checked @endif
                                                          value="1">
                                                    <span>Is Featured?</span>
                                                </label>

                                            </div>
                                        </div>

                                        {{-- photo upload --}}

                                        <div class="col-12">


                                            <img src="{{ asset('public/uploads/'.$category->photo) }}" height="100"
                                                alt="Image">
                                            <div class="form-group mt-3 mb-5">
                                                <input type="file" name="photo" class="dropify">
                                                <small id="fileHelp" class="form-text text-muted">This is some
                                                    placeholder block-level help text for the above input. It's a bit
                                                    lighter and easily wraps to a new line.</small>
                                            </div>
                                        </div>


                                        <!-- hidden id -->

                                        <input type="hidden" name="id" value="{{$category->id}}">

                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Update</button>

                                            <a class="btn btn-secondary" href="{{url('category/categories')}}">
                                                CLOSE</a>
                                        </div>
                                    </div>
                                    @if($errors->any())
                                    {!! implode('', $errors->all("<div class='error'>
                                        <p style='color:red'>:message</p>
                                    </div>")) !!}
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
