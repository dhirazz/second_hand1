<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['auth','rbac'],'prefix'=>'category'],function() {
    // Route::get('/categories', 'CategoryController@index');
    // Route::get('/create', 'CategoryController@create');
    // Route::post('/store', 'CategoryController@store');
    // Route::get('/edit/{id}', 'CategoryController@edit');
    // Route::post('/update', 'CategoryController@update');
    // Route::post('/destroy', 'CategoryController@destroy');


    Route::resource('category','CategoryController');

    Route::post('delete','CategoryController@delete')->name('category.delete');

    Route::get('search','CategoryController@search')->name('category.search');
});
