<form data-route="{{ route('maincategory.search') }}" id="main-category-form">

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <input type="text" class="form-control main-category-keyword" placeholder="Search ..." name="keyword">
            </div>

        </div>

        <div class="col-md-3">
            <div class="form-group">
                <input type="text" value="{{ @$date_from }}" data-provide="datepicker" readonly class="form-control date_from_category datepicker" placeholder="Date From " name="date_from" >
            </div>
        </div>

        <div class="col-md-3">

            <div class="form-group">
                <input type="text" value="{{ @$date_to }}" data-provide="datepicker" readonly class="form-control date_to_category category-keyword datepicker" placeholder="Date To" name="date_to">
            </div>
        </div>
    </div>

</form>
