<form action="{{route('maincategory.store')}}" enctype="multipart/form-data" method="post">
    @csrf
    <div class="row clearfix">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="maincategory_name" class="input-group-text">maincategory</label>
                    </div>
                    <input type="text" name="main_category" id="maincategory_name"
                        class="form-control show-tick @error('main_category') is-invalid @enderror"
                        placeholder="maincategory *" value="{{old('main_category')}}"
                        required="">
                </div>
            </div>
        </div>

        @error('main_category')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror


        {{-- is featured --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <label class="fancy-checkbox">
                    <input class="checkbox-tick" type="checkbox"
                        name="featured" value="1">
                    <span>Is Featured?</span>
                </label>

            </div>
        </div>


        {{-- photo upload --}}

        <div class="col-12">
            <div class="form-group mt-3 mb-5">
                <input type="file" name="photo" class="dropify">
                <small id="fileHelp" class="form-text text-muted">
            </div>
        </div>


        <div class="col-12">
            <button type="submit" class="btn btn-primary">Add</button>
            <a class="btn btn-secondary"
                href="{{route('maincategory.index')}}">
                CLOSE
            </a>
        </div>

    </div>

    @if($errors->any())
    {!! implode('', $errors->all("<div class='error'>
        <p style='color:red'>:message</p>
    </div>")) !!}
    @endif
</form>
