@extends('layouts.admin.main')




@section('title')
Main Categories
@endsection
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Main Categories
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">

            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#Users"> Main Categories
                            </a></li>
                            @if(in_array('maincategory.store',session('permission')))

                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Add New maincategory
                            </a></li>
                            @endif
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">
                             <!-- search -->

            @include('maincategory::admin.search')
                            <div class="table-responsive">

                                <form method="post">
                                    @csrf
                                    @if(in_array('maincategory.delete',session('permission')))

                                    <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('maincategory.delete') }}" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>

                                    @endif
                                    <table class="table table-hover table-custom spacing8"
                                    id="table-maincategory">
                                        <thead>
                                            <tr>
                                                @if(in_array('maincategory.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick" type="checkbox" id="check_all">
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                                @endif
                                                <th>Main Category</th>

                                                @if(in_array('maincategory.edit',session('permission')))

                                                    <th>Actions</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($main_categories as $value)

                                            <tr>
                                                @if(in_array('maincategory.delete',session('permission')))

                                                    <td>

                                                        <label class="fancy-checkbox">
                                                            <input class="checkbox-tick checkitem" type="checkbox"
                                                                name="slug[]" value="{{ $value->slug }}">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                @endif

                                                <td>
                                                    <h6 class="mb-0"> {{$value->main_category}}</h6>
                                                </td>

                                                @if(in_array('maincategory.edit',session('permission')))


                                                    <td>
                                                        <a href="{{route('maincategory.edit',$value->slug)}}">
                                                            <i class="icon-pencil"></i>
                                                        </a>

                                                    </td>

                                                @endif

                                                @endforeach
                                        </tbody>
                                    </table>
                                </form>
                                {{-- {!! $Main Categories->render() !!} --}}

                            </div>
                        </div>

                        @if(in_array('maincategory.store',session('permission')))

                            <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                                <div class="body mt-2">

                                    @include('maincategory::admin.create')
                                </div>
                            </div>

                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>




@endsection
