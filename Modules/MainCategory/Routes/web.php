<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('maincategory')->group(function() {
    Route::resource('maincategory','MainCategoryController');

    Route::post('maincategory/delete','MainCategoryController@delete')->name('maincategory.delete');
    Route::get('search','MainCategoryController@search')->name('maincategory.search');

});
