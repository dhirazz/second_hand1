<?php

namespace Modules\MainCategory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\MainCategory\Entities\MainCategory;
use File;

class MainCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $main_categories = MainCategory::orderBy('main_category')->get();
        return view(
            'maincategory::admin.index',
            compact('main_categories')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('maincategory::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'main_category' => 'required|unique:main_categories,main_category'
        ];

        $request->validate($rules);

        $main_category = new MainCategory();
        $main_category->main_category = $request->main_category;
        $main_category->slug = date('ymdhis') . rand(0, 9999);

        if ($request->hasFile('photo')) {
            //upload new one
            $file = $request->file('photo');
            $photo = time() . '.' . $file->getClientOriginalExtension();
            $destination_path = public_path('/uploads');
            $file->move($destination_path, $photo);

            $main_category->photo = $photo;
        }
        $main_category->save();

        return back();
    }

  

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $main_category = MainCategory::where('slug',$id)->first();
        return view('maincategory::admin.edit',compact('main_category'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {

        $main_category = MainCategory::where('slug',$id)->first();


        $id = $main_category->id;

        $rules = [
            'main_category' => 'required|unique:main_categories,main_category,' . $id
        ];

        $request->validate($rules);

        $main_category->main_category = $request->main_category;

        if($request->featured){
            $main_category->featured = $request->featured;
        }else{
            $main_category->featured = '0';

        }
        // $main_category->slug = date('ymdhis').rand(0,9999);

        if ($request->hasFile('photo')) {

            //remove file from the folder

            $old_file = public_path('/uploads') . '/' . $category->photo;

            if (File::exists($old_file)) {
                File::delete($old_file);
            }

            //upload new one
            $file = $request->file('photo');
            $photo = time() . '.' . $file->getClientOriginalExtension();
            $destination_path = public_path('/uploads');
            $file->move($destination_path, $photo);

            $main_category->photo = $photo;
        }
        $main_category->save();

        return redirect()->route('maincategory.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete(Request $request)
    {
        if ($request->slug) {
            $main_categories = MainCategory::whereIn('slug', $request->slug)

                ->get();

            foreach ($main_categories as $value) {
                $old_file = public_path('/uploads') . '/' . $value->photo;

                if (File::exists($old_file)) {
                    File::delete($old_file);
                }

                $value->delete();
            }
        }

        return back();
    }


    //search

    public function search(Request $request){

        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $keyword = $request->keyword;

        $main_categories = MainCategory::where('main_category', 'like', '%' .$keyword . '%');

        if($request->date_from && $request->date_to){

            $date_from = date('Y-m-d',strtotime($date_from));
            $date_to = date('Y-m-d',strtotime($date_to));
        }

        $main_categories = $main_categories->orderBy('main_category')->get();

        return response()->json($main_categories);
    }
}
