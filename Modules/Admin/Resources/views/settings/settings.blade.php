@extends('layouts.admin.main')

@section('title')
Settings
@endsection
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Settings</h2>
                </div>

            </div>
        </div>

        <div class="row clearfix">

            <div class="col-xl-8 col-lg-8 col-md-7">
                <form action="{{url('admin/change-settings')}}" method="post" enctype="multipart/form-data">

                    <div class="card">

                        <div class="body">

                            @csrf
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Company Name</span>
                                            </div>
                                            <input type="text" class="form-control" name="company_name"
                                                value="{{old('company_name',$setting->company_name)}}"
                                                placeholder="Company Name" required>
                                        </div>
                                    </div>
                                </div>


                                {{-- Phone --}}
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Phone</span>
                                            </div>
                                            <input type="text" class="form-control" name="phone"
                                                value="{{old('phone',$setting->phone)}}" placeholder="Phone" required>
                                        </div>
                                    </div>
                                </div>


                                <!-- email -->
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Email</span>
                                            </div>
                                            <input type="email" class="form-control" name="email"
                                                value="{{old('email',$setting->email)}}" placeholder="Email" required>
                                        </div>
                                    </div>
                                </div>

                                <!-- facebook -->
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Facebook</span>
                                            </div>
                                            <input type="text" class="form-control" name="facebook"
                                                value="{{old('facebook',$setting->facebook)}}" placeholder="Facebook"
                                                required>
                                        </div>
                                    </div>
                                </div>

                                <!-- twitter -->
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Twitter</span>
                                            </div>
                                            <input type="text" class="form-control" name="twitter"
                                                value="{{old('twitter',$setting->twitter)}}" placeholder="Twitter"
                                                required>
                                        </div>
                                    </div>
                                </div>

                                <!-- instagram -->

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Instagram</span>
                                            </div>
                                            <input type="text" class="form-control" name="instagram"
                                                value="{{old('instagram',$setting->instagram)}}" placeholder="Instagram"
                                                required>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">


                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Date</span>
                                            </div>
                                            <div class="col-md-8">
                                                <select name="date_type" class="date" required id="">
                                                    <option value="">--Select Date--</option>
                                                    <option value="English" @if ($setting->date_type=='English')
                                                        selected
                                                        @endif
                                                        >English</option>
                                                    <option value="Nepali" @if ($setting->date_type=='Nepali')
                                                        selected
                                                        @endif
                                                        >Nepali</option>
                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                </div>


                                <!-- address -->

                                <div class="col-lg-12 col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Address</span>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <textarea rows="4" name="address" type="text" class="form-control"
                                            placeholder="Address">{{old('address',$setting->address)}}</textarea>
                                    </div>
                                </div>




                            </div>
                            <button type="submit" class="btn btn-round btn-primary">Update</button> &nbsp;&nbsp;
                            <a href="{{url('user/settings')}}" class="btn btn-round btn-default">Cancel</a>

                            <!-- session messages -->
                        </div>
                    </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-5">

                <div class="card">
                    <div class="body">

                        <p>Official Logo</p>

                        <div>
                            <img width="100%" height="100" style="object-fit: contain"
                                src="{{asset('public/uploads').'/'.$setting->company_logo}}" alt="">
                        </div>


                        {{-- file upload code --}}
                        <br>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                {{-- <span class="input-group-text">Upload</span> --}}
                            </div>
                            <div class="custom-file">
                                <input type="file" name="company_logo" class="custom-file-input" id="inputGroupFile01">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                            </div>
                        </div>
                        <hr>
                        <small class="text-muted">Address: </small>
                        <p>{{$setting->address}}</p>
                        <div>
                            <iframe width="100%" height="150" frameborder="0" style="border:0" allowfullscreen
                                src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=
                                    {{str_replace(',', '', str_replace(' ', '+', $setting->address))}}&z=14&output=embed">
                            </iframe>


                        </div>
                        <hr>
                    </div>
                </div>


            </div>
        </form>


        </div>



    </div>
</div>

<!-- include jquery -->
<script src="{{asset('public/js/jquery.min.js')}}"></script>

<script>
    // select2 for branch

    $(document).ready(function() {
        $('.date').select2({
            placeholder: "Select Date",
            theme: "material",
            allowClear: true
        });

     
    });

  


</script>
@endsection