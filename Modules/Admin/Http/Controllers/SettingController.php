<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Setting;

use File;
use Illuminate\Support\Facades\DB;
use Session;

class SettingController extends Controller
{
    //

    public function setting(){
        $setting = Setting::find(1);

        // $payment_statuses = DB::table('payment_statuses')->get();

        // $taxes = DB::table('taxes')->get();

        // if(Session::get('stored_role_id')){

        //     return view('user::other-user.settings.settings',compact('setting','payment_statuses','taxes'));

        // }
        return view('admin::settings.settings',compact('setting'));
    }

    public function changeSetting(Request $request){

        $rules = [
            'company_name'=>'required',
            'company_logo' =>'mimes:jpg,png,gif,jpeg'
        ];

        $request->validate($rules);

        $setting = Setting::find(1);

        $setting->company_name = $request->company_name;


        $setting->date_type = $request->date_type;

        if($request->hasFile('company_logo')){

            $file = $request->file('company_logo');

            $old_path = public_path().'/uploads/'.$setting->company_logo;


            if(File::exists($old_path)){
                File::delete($old_path);
            }

            $path = public_path().'/uploads';

            $filename = date('Ymdhis').'-'.$file->getClientOriginalName();

            $file->move($path,$filename);

            $setting->company_logo = $filename;

        }
        $setting->email = $request->email;
        $setting->phone = $request->phone;
        $setting->address = $request->address;
        $setting->facebook = $request->facebook;
        $setting->twitter = $request->twitter;
        $setting->instagram = $request->instagram;


        $setting->save();

        Session::flash('message', 'Settings Updated Successfully!'); 
        Session::flash('alert-class', 'alert-success'); 
        return back();

    }
}
