<?php

namespace Modules\User\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Role\Entities\Role;

use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $user_id = auth()->user()->id;

        $user_role = auth()->user()->role_id;

        $role = Role::find($user_role);

        if($role->type=='superadmin'){
            $users = User::select(['*', 
            'users.created_at as created_at', 'users.id as id',
                'roles.name as role_name',
                'users.name as name'
            ])
                ->leftjoin('roles', 'users.role_id', '=', 'roles.id')
                ->where('users.id', '!=', $user_id)
                ->orderBy('users.id', 'DESC')
                ->paginate(config('number_of_pages'));

                $roles = Role::all();

        }else{
            $users = User::select(['*', 
            'users.created_at as created_at', 'users.id as id',
                'roles.name as role_name',
                'users.name as name'
            ])
                ->join('roles', 'users.role_id', '=', 'roles.id')
                ->where('users.id', '!=', $user_id)
                ->where('roles.type', '!=', 'superadmin')
                ->orderBy('users.id', 'DESC')
                ->paginate(config('number_of_pages'));

                $roles = Role::where('roles.type', '!=', 'superadmin')
                ->get();
        }  




        return view('user::admin.index',
            compact('users', 'roles','role')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('user::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'username' => ['required', 'string', 'max:255', 'unique:users,username'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'mobile' => 'required|numeric|digits:10|unique:users,mobile'
        ];

        $request->validate($rules);

        //save user

        $user = new User;
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->username = $request->username;

        $user->role_id = $request->role_id;

        $user->password = Hash::make($request->password);

        $user->profile_code = md5($user->password).date('ymdhis');

        $user->save();

        return back();
    }

   

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
      
        $user = User::where('profile_code',$id)->first();

        $user_id = auth()->user()->id;

        $user_role = auth()->user()->role_id;

        $role = Role::find($user_role);

        if($role->type=='superadmin'){
            $roles = Role::all();
        }else{
            $roles = Role::where('roles.type', '!=', 'superadmin')
            ->get();
        }

      


        return view('user::admin.edit',
            compact('user', 'roles')

        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $user = User::where('profile_code',$id)->first();

        $id= $user->id;


        $rules = [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'email' => "required|string|email|max:255|unique:users,email,".$id,
            'username' => "required|string|max:255|unique:users,username,".$id,
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,'.$id
        ];

        $request->validate($rules);


        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role_id = $request->role_id;

        $user->save();

        Session::flash('message', 'User Updated Successfully!');

        Session::flash('alert-class', 'alert-success');

        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request){

        if($request->id){
            $user = User::whereIn('id',$request->id)->delete();
        }

        return back();

    }
}
