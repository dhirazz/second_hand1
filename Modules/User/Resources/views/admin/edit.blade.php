@extends('layouts.admin.main')

@section('title')
Edit User
@endsection

@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Edit User</h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#addUser">Edit
                                User</a></li>
                    </ul>
                    <div class="tab-content mt-0">

                        <div class="tab-pane active show" id="addUser">
                            <div class="body mt-2">
                                <form action="{{route('user.update',$user->profile_code)}}" method="post">
                                    @csrf
                                    @method('PUT')

                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Full Name</span>
                                                    </div>

                                                    <input type="text"
                                                        class="form-control @error('name') is-invalid @enderror"
                                                        value="{{old('name',$user->name)}}"
                                                        name="name" placeholder="Full Name *">

                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Email</span>
                                                    </div>

                                                    <input type="email"
                                                        class="form-control @error('email') is-invalid @enderror"
                                                        name="email" value="{{old('email',$user->email)}}"
                                                        placeholder="Email *" required>

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Mobile</span>
                                                    </div>

                                                    <input type="text"
                                                        class="form-control @error('mobile') is-invalid @enderror"
                                                        value="{{old('mobile',$user->mobile)}}" name="mobile"
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                        placeholder="Mobile No" required>

                                                    @error('mobile')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6  col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Username</span>
                                                    </div>

                                                    <input type="text" name="username"
                                                        value="{{old('username',$user->username)}}"
                                                        class="form-control @error('username') is-invalid @enderror"
                                                        placeholder="Username *" required="">
                                                    @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        @php
                                        // $role_id = auth()->user()->role_id;
                                        //$role= \App\Role::find($role_id)->type;

                                        $user_id = auth()->user()->id;



                                        @endphp

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Role</span>
                                                    </div>

                                                    <div class="col-md-8 col-lg-8">


                                                        <select
                                                            class="form-control role_id show-tick @error('role_id') is-invalid @enderror"
                                                            name="role_id" required>
                                                            <option value="">Select Role Type</option>

                                                            @foreach ($roles as $value)
                                                            <option
                                                                {{(old('role_id',$user->role_id)==$value->id)? 'selected':''}}
                                                                value="{{$value->id}}">
                                                                {{$value->name}}</option>
                                                            @endforeach

                                                        </select>
                                                        @error('role_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <a class="btn btn-secondary" href="{{route('user.index')}}">
                                                CLOSE
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection