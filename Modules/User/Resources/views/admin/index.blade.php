@extends('layouts.admin.main')

@section('title')
    Users
@endsection

@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>User List</h2>
                </div>
            </div>
        </div>
        <div class="row clearfix">



            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif" data-toggle="tab" href="#Users">Users</a></li>

                        @if(in_array('user.store',session('permission')))

                            <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif" data-toggle="tab" href="#addUser">Add User</a></li>
                        @endif

                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane  @if(!session()->get('errors')) active show @endif" id="Users">

                            @include('user::admin.search')

                            <div class="table-responsive">
                                <form method="post">
                                    @csrf
                                    @if(in_array('user.delete',session('permission')))

                                     <button class="btn btn-danger"
                                      onclick="return confirm('Do you really want to delete this item?')"
                                       formaction="{{ route('user.delete') }}" type="submit">
                                     <i class="fa fa-trash"></i>
                                    </button>

                                    @endif
                                <table class="table table-hover table-custom spacing8">
                                    <thead>
                                        <tr>
                                            @if(in_array('user.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick" type="checkbox" id="check_all"
                                                        >
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                            @endif
                                            <th class="w60">Name</th>
                                            <th></th>
                                            <th>Role</th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Created Date</th>

                                            @if(in_array('user.edit',session('permission')))
                                                <th>Action</th>

                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($users as $value)

                                        <tr>
                                            @if(in_array('user.delete',session('permission')))
                                            <td>

                                                <label class="fancy-checkbox">
                                                    <input class="checkbox-tick checkitem"
                                                   type="checkbox"
                                                   name="id[]" value="{{ $value->id }}">
                                                   <span></span>
                                               </label>
                                            </td>
                                            @endif
                                            <td class="width45">
                                                <div class="avtar-pic w35 bg-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$value->first_name.' '.$value->last_name}}">
                                                    @if($value->photo)

                                                    <img src="{{asset('public/uploads/'.$value->photo)}}" alt="" height="35px" width="35px">
                                                    @else
                                                    <span>
                                                        <img height="35px" width="35px" src="{{asset('public/uploads/avatar.webp')}}" alt="">

                                                    </span>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <h6 class="mb-0">{{$value->name}}</h6>
                                                <span>{{$value->email}}</span>
                                            </td>
                                            <td><span class="badge badge-danger">{{$value->role_name}}</span></td>

                                            <td>{{$value->email}}</td>


                                            <td> {{$value->username}} </td>


                                            <td>{{date('d M, Y',strtotime($value->created_at))}}</td>

                                            @if(in_array('user.edit',session('permission')))

                                                <td>


                                                    @if($role->type=='superadmin')
                                                        <a href="{{route('profile.view',$value->profile_code)}}">
                                                            <i class="fa fa-eye"></i>
                                                        </a>
                                                    @endif



                                                    <a href="{{route('user.edit',$value->profile_code)}}"> <i class="icon-pencil"></i> </a>
                                                    </a>
                                                </td>

                                            @endif
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </form>

                                {!! $users->render() !!}
                            </div>
                        </div>

                        @if(in_array('user.store',session('permission')))

                            <div class="tab-pane @if(session()->get('errors'))  active show  @endif" id="addUser">
                                <div class="body mt-2">

                                <form action="{{route('user.store')}}" method="post">
                                    @csrf
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Full Name</span>
                                                    </div>

                                                    <input type="text" class="form-control @error('name') is-invalid @enderror"
                                                        value="{{old('name')}}" name="name" placeholder="Full Name *">

                                                    @error('name')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Email</span>
                                                    </div>
                                                    <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                                        value="{{old('email')}}" placeholder="Email *" required>

                                                    @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        {{-- Password --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Password</span>
                                                    </div>
                                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                                        value="{{old('password')}}" placeholder="Password" name="password" required>
                                                    @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Confirm Password</span>
                                                    </div>
                                                    <input type="password" class="form-control @error('password_confirmation') is-invalid @enderror"
                                                        placeholder="Confirm Password" name="password_confirmation" required>
                                                    @error('password_confirmation')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Mobile</span>
                                                    </div>
                                                    <input type="text" class="form-control @error('mobile') is-invalid @enderror"
                                                        oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                                        value="{{old('mobile')}}" name="mobile" placeholder="Mobile No" required>

                                                    @error('mobile')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        {{-- <div class="col-lg-3 col-md-4 col-sm-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Employee ID *">
                                        </div>
                                    </div> --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Username</span>
                                                    </div>
                                                    <input type="text" name="username" value="{{old('username')}}"
                                                        class="form-control @error('username') is-invalid @enderror" placeholder="Username *"
                                                        required="">
                                                    @error('username')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        {{-- Role --}}
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Role</span>
                                                    </div>
                                                    <div class="col-md-7 col-lg-7">
                                                        <select class="form-control role_id show-tick @error('role_id') is-invalid @enderror"
                                                            name="role_id" required>
                                                            <option value="">--Select Role--</option>

                                                            @foreach ($roles as $value)
                                                            <option value="{{$value->id}}"

                                                                {{ (Request::old("role_id") == $value->id ? "selected":"") }}
                                                                >{{$value->name}}</option>

                                                            @endforeach

                                                        </select>
                                                        @error('role_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>






                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                            <a class="btn btn-secondary" href="{{route('user.index')}}">
                                                CLOSE
                                            </a>
                                        </div>
                                    </div>
                                </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
