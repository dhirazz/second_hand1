@extends('layouts.admin.main')

@section('title')
    Permissions
@endsection
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
            <div class="col-md-6 col-sm-12">
                    <h2>Permissions</h2>
                </div>       
            </div>
        </div>
        <div class="row clearfix">

        <!-- for search -->
        {{-- @include('user::search.permission-search') --}}
        
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif" data-toggle="tab" href="#Users">Permissions</a></li>
                        <li class="nav-item">
                            <a class="nav-link @if(session()->get('errors')) active show @endif" data-toggle="tab" href="#addUser">
                                Add Permission
                            </a>
                        </li>        
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">
                        <div class="table-responsive">

                            <form method="post">
                                @csrf
                                <button class="btn btn-danger"
                                 onclick="return confirm('Do you really want to delete this item?')"
                                  formaction="{{ route('permission.delete') }}" type="submit"> 
                                     <i class="fa fa-trash"></i>
                               </button>
                                <table class="table table-hover table-custom spacing8">           
                                    <thead>
                                        <tr>
                                             <th> 
                                                <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" id="check_all"
                                                    >
                                                    <span>#</span>
                                                </label>
                                            </th>
                                            <th>Module Permission</th>
                                            <th></th>
                                            <th>Created Date</th>
                                            <th class="w100">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($permissions as $value)
                                            <tr>
                                                 <td>

                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick checkitem" 
                                                       type="checkbox"
                                                       name="module[]" value="{{ $value->module }}">
                                                       <span></span>
                                                   </label>
                                                </td>
                                          
                                                <td>
                                                <h6 class="mb-0">{{$value->module}}</h6>
                                                </td>
                                                <th></th>
                                                <td>
                                                    
                                                    {{date('d M, Y',strtotime($value->created_at))}}
                                                
                                                </td>
                                                <td>
                                                    <a href="{{route('permission.edit',$value->permission_code)}}">
                                                        
                                                       <i class="icon-pencil"></i>                                                      </a> 
                                                    </td>
                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table> 
                                </form>

                                
                                {!! $permissions->render() !!}

                            </div>                        </div>
                        <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                            <div class="body mt-2">
                                <form action="{{route('permission.store')}}" method="post">
                                    @csrf
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Module</span>
                                                </div>
                                
                                            <input type="text" name="module" 
                                            class="form-control show-tick @error('module') 
                                            is-invalid @enderror"
                                             placeholder="Module Name *" value="{{old('module')}}" required="">
                                
                                             @error('module')
                                             <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                             </span>
                                             @enderror
                                        </div>
                                        </div>
                                    </div>
                                
                                
                                    <div class="col-12">
                                        <button type="submit" class="btn btn-primary">Add</button>
                                        <a href="{{url('permission/permissions')}}" class="btn btn-secondary">
                                            CLOSE
                                        </a>    
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>
    </div>
</div>

@endsection