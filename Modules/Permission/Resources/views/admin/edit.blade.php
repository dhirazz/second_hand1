@extends('layouts.admin.main')

@section('title')
    Edit Permission
@endsection
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Permissions</h2>
                </div>            
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active show" data-toggle="tab" href="#addUser">Edit Permission</a>
                        </li>        
                    </ul>
                    <div class="tab-content mt-0">
                        
                        <div class="tab-pane active show" id="addUser">
                            <div class="body mt-2">
                                <form action="{{route('permission.update',$permission->permission_code)}}" method="post">
                                    @csrf
                                    @method('PUT')

                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Module</span>
                                                    </div>
                                                    <input type="text" name="module" class="form-control show-tick @error('module') 
                                              is-invalid @enderror" placeholder="Permission Link *" value="{{old('module',$permission->module)}}"
                                                        required="">
                                
                                
                                
                                                    @error('module')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                
                                
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Update</button>
                                            <a href="{{route('permission.index')}}" class="btn btn-secondary">
                                                CLOSE
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>            
                </div>
            </div>
        </div>
    </div>
</div>

@endsection