<?php

namespace Modules\Permission\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Permission\Entities\Permission;
use Session;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {

        $permissions = Permission::orderBy('module','asc')
        ->groupby('module')
        ->paginate(config('number_of_pages'));


        return view('permission::admin.index',
            compact('permissions')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('permission::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'module' => 'required|unique:permissions,module',
            //'link' =>'required|unique:permissions,link'
        ];

        $request->validate($rules);


        $module = strtolower($request->module);

        // create permission
        $permission = new Permission;
        $permission->module = $module;
        $permission->type = 'create';
        $route = $module . '.' . 'store';
        $permission->link = $route;

        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);

        $permission->save();


        //view list permission
        $permission = new Permission;
        $permission->module = $module;
        $route = $module . '.' .'index';
        $permission->type = 'view';
        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);

        $permission->save();


        //edit permission
        $permission = new Permission;
        $permission->module = $module;
        $permission->type = 'edit';
        $route = $module . '.' . 'edit';
        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);

        $permission->save();

        // Delete permission

        $permission = new Permission;
        $permission->module = $module;
        $permission->type = 'delete';

        $route = $module . '.' . 'delete';

        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);


        $permission->save();

        return redirect()->back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('permission::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $permission = Permission::where('permission_code',$id)->first();

        return view('permission::admin.edit',
            compact('permission')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'module' => 'required',
        ];

        $request->validate($rules);

        $permission = Permission::where('permission_code',$id)->first();

        $module = $permission->module;
  

        $permission = Permission::where('module',$module)->delete();
   

        $module = strtolower($request->module);

        // create permission
        $permission = new Permission;
        $permission->module = $module;
        $permission->type = 'create';
        $route = $module . '.' . 'store';
        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);
        $permission->save();


        //view list permission
        $permission = new Permission;
        $permission->module = $module;
        $route = $module . '.' .'index';
        $permission->type = 'view';
        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);
        $permission->save();


        //edit permission
        $permission = new Permission;
        $permission->module = $module;
        $permission->type = 'edit';
        $route = $module . '.' . 'edit';
        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);
        $permission->save();

        // Delete permission

        $permission = new Permission;
        $permission->module = $module;
        $permission->type = 'delete';
        $route = $module . '.' . 'delete';
        $permission->link = $route;
        $permission->permission_code = md5($permission->link).''.date('ymdhis').''.rand(0,9999999);
        $permission->save();


        Session::flash('message', 'Permission Updated Successfully!');

        Session::flash('alert-class', 'alert-success');

        return redirect()->route('permission.index');
    }


    public function delete(Request $request){
        if($request->module){
            $permission = Permission::whereIn('module',$request->module)->where('permissions.type','!=','settings')->delete();
        }

        return redirect()->route('permission.index');

    }
}
