<?php

namespace Modules\Profile\Http\Controllers;

use App\Visitor;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Profile\Entities\UserProfile;
use Auth;
use File;
use Session;

class VisitorProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        @$countries = file_get_contents('https://api.printful.com/countries');


        // convert json to laravel eloquent

        @$countries = (array)json_decode($countries);


        // $countries = json_decode($countries);

        @$countries = $countries['result'];


        if(Auth::guard('visitor')->check()){
            $id = Auth::guard('visitor')->user()->id;

        }


        $profile = UserProfile::where('user_id',$id)->first();
        $user = Visitor::find($id);

        if(!$profile){
            $profile = new UserProfile();
        }
        return view('profile::front.index',compact('profile','user','countries'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {

        $user = Visitor::where('id',$id)->first();


        $rules = [
            'name'=>'required',
            'email'=>'required|unique:visitors,email,'.$id,
            'mobile'=>'required|unique:visitors,mobile,'.$id ,
            'username'=>'required|unique:visitors,username,'.$id,
            'dob'=>'required',
            'country'=>'required',
            'state'=>'required',
            'city'=>'required',
            'address' => 'required',
            'gender' =>'required',
            'photo' => 'mimes:jpeg,jpg,png,gif'

        ];

        $names = [
            'dob'=>'Date Of Birth'
        ];

        $request->validate($rules,[],$names);


        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->save();

        //check admin profile

        $profile = UserProfile::where('user_id',$id)->first();

        if(!$profile){
            $profile = new UserProfile;
        }

        $profile->user_id = $user->id;
	    $profile->gender = $request->gender;
	    $profile->dob	= date('Y-m-d',strtotime($request->dob));
	    $profile->website = $request->website;
        $profile->country = $request->country;
	    $profile->state = $request->state;
	    $profile->city = $request->city;
	    $profile->facebook = $request->facebook;
	    $profile->instagram = $request->instagram;
        $profile->twitter = $request->twitter;
	    $profile->address = $request->address;
        //image upload code

        if ($request->hasFile('photo')) {

            $file = $request->file('photo');

            $old_path = public_path() . '/uploads/' . $profile->photo;

            if (File::exists($old_path)) {
                File::delete($old_path);
            }

            $path = public_path() . '/uploads';

            $filename = date('Ymdhis') . '-' . $file->getClientOriginalName();

            $file->move($path, $filename);

            $profile->photo = $filename;

        }
        $profile->save();

        Session::flash('message', 'Profile Updated Successfully!');
        Session::flash('alert-class', 'alert-success');


        return back();
    }
}
