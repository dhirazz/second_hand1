<?php

namespace Modules\Profile\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Session;

class ChangePasswordController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $id = auth()->user()->id;
        $user = User::find($id);

        return view('profile::admin.change-password',
        compact('user')
        );

    }



    public function update(Request $request, $id)
    {
        $rules = [
            'old_password'=>'required',
            'password' => ['required', 'string', 'min:8', 'confirmed'],

        ];

        $request->validate($rules);

        $user = User::where('profile_code',$id)->first();

        if (Hash::check($request->old_password, $user->password)) { 

            $user->password = Hash::make($request->password);
            
            $user->save();

            Session::flash('message', 'Password Changed Successfully!'); 

            Session::flash('alert-class', 'alert-success');

        }else{
            return back()->withErrors(['old_password'=>'Incorrect Old Password']);

        }

        return back();

    }

}
