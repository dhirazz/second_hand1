<?php

namespace Modules\Profile\Http\Controllers;

use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Profile\Entities\AdminProfile;
use Illuminate\Support\Facades\Hash;

use Session;
use File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */

    //update own account profile
    public function index()
    {
        $id = auth()->user()->id;


        $user = User::find($id);

        $profile = AdminProfile::where('user_id',$id)->first();


        if($profile){
            $profile->dob = date('m/d/Y',strtotime($profile->dob));
        }

        $countries = file_get_contents('https://api.printful.com/countries');


        // convert json to laravel eloquent

        $countries = (array)json_decode($countries);


        // $countries = json_decode($countries);

        $countries = $countries['result'];


        return view('profile::admin.index',
            compact('profile','user','countries')
        );
    }

    public function update(Request $request,$id)
    {

        $user = User::where('profile_code',$id)->first();

        $id=$user->id;


        $rules = [
            'name'=>'required',
            'email'=>'required|unique:users,email,'.$id,
            'mobile'=>'required|unique:users,mobile,'.$id ,
            'username'=>'required|unique:users,email,'.$id,
            'dob'=>'required',
            'country'=>'required',
            'state'=>'required',
            'city'=>'required',
            'address' => 'required',
            'gender' =>'required',
            'photo' => 'mimes:jpeg,jpg,png,gif'

        ];

        $names = [
            'dob'=>'Date Of Birth'
        ];

        $request->validate($rules,[],$names);


        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->save();

        //check admin profile

        $admin = AdminProfile::where('user_id',$id)->first();

        if(!$admin){
            $admin = new AdminProfile;
        }

        $admin->user_id = $user->id;
	    $admin->gender = $request->gender;
	    $admin->dob	= date('Y-m-d',strtotime($request->dob));
	    $admin->website = $request->website;
        $admin->country = $request->country;
	    $admin->state = $request->state;
	    $admin->city = $request->city;
	    $admin->facebook = $request->facebook;
	    $admin->instagram = $request->instagram;
        $admin->twitter = $request->twitter;
	    $admin->address = $request->address;
        //image upload code

        if ($request->hasFile('photo')) {

            $file = $request->file('photo');


            $old_path = public_path() . '/uploads/' . $admin->photo;

            if (File::exists($old_path)) {
                File::delete($old_path);
            }

            $path = public_path() . '/uploads';

            $filename = date('Ymdhis') . '-' . $file->getClientOriginalName();

            $file->move($path, $filename);

            $admin->photo = $filename;

        }
        $admin->save();

        Session::flash('message', 'Profile Updated Successfully!');
        Session::flash('alert-class', 'alert-success');

        return back();
    }

    // change password

    public function changePassword(Request $request,$id){

    }


    /*
        Update other's Account information
    */

    public function viewProfile($id){

        $user = User::where('profile_code',$id)->first();

        $id = $user->id;

        $profile = AdminProfile::where('user_id',$id)->first();

        if($profile){
            $profile->dob = date('m/d/Y',strtotime($profile->dob));
        }

        $countries = file_get_contents('https://api.printful.com/countries');


        // convert json to laravel eloquent

        $countries = (array)json_decode($countries);



        $countries = $countries['result'];

        return view('profile::admin.index',
            compact('profile','user','countries')
        );

    }
}
