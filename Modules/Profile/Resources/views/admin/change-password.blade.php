@extends('layouts.admin.main')

@section('title')
Change Password
@endsection

@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Change Password</h2>
                </div>

            </div>
        </div>
        <div class="row clearfix">


            <div class="col-xl-8 col-lg-8 col-md-7">


                <div class="card">
                    <div class="body">

                        @if(Session::has('message'))
                        <p class="alert alert-info">{{ Session::get('message') }}</p>
                        @endif


                        <form action="{{route('change-password.update',$user->profile_code)}}" method="post">
                            <div class="row clearfix">
                                @csrf
                                @method('PUT')
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <input type="password" name="old_password" class="form-control"
                                            placeholder="Current Password" value="{{ old('old_password') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" class="form-control"
                                            placeholder="New Password"  value="{{ old('password') }}" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password_confirmation" class="form-control"
                                            placeholder="Confirm New Password">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-round btn-primary">Update</button> &nbsp;&nbsp;
                            <a href="{{route('admin')}}" class="btn btn-round btn-default">Cancel</a>

                        </form>

                    </div>

                    @if($errors->any())
                    {!! implode('', $errors->all("<div class='error'>
                        <p style='color:red'>:message</p>
                    </div>")) !!}
                    @endif
                </div>
            </div>


        </div>
    </div>
</div>

@endsection