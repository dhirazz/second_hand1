
@section('title')
    User Profile
@endsection


@extends('layouts.front.main')

@section('content')

<div class="product-area section">
    <div class="container">

        <div class="row clearfix">


            <div class="col-xl-8 col-lg-8 col-md-7">
                <div class="card">
                    <div class="header">
                        <h2>Basic Information</h2>
                    </div>
                    <div class="body">

                    @if(Session::has('message'))
                    <p class="alert alert-info">{{ Session::get('message') }}</p>
                    @endif

                        <form action="{{ route('visitor.profile.post',$user->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row clearfix">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <input type="text" value="{{$user->name}}" class="form-control" name="name"
                                            placeholder="Full Name" required>
                                    </div>
                                </div>

                                <!-- Email -->
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <input type="email" value="{{old('email',$user->email)}}" class="form-control"
                                            placeholder="Email" name="email" required>
                                    </div>
                                </div>

                                <!-- username -->

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="username" value="{{old('username',$user->username)}}"
                                            class="form-control" placeholder="Username" required>
                                    </div>
                                </div>
                                <!-- photo -->
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <input type="file" name="photo">
                                    </div>
                                </div>

                                <!-- Mobile -->

                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="mobile" value="{{old('mobile',$user->mobile)}}"
                                            oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                            class="form-control" placeholder="Mobile" required>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <select class="form-control" name="gender">
                                            <option value="">-- Select Gender --</option>
                                            <option value="male"
                                                {{ old('gender',@$profile->gender)=='male' ? 'selected' : ''  }}>Male
                                            </option>
                                            <option value="female"
                                                {{ old('gender',@$profile->gender)=='female' ? 'selected' : ''  }}>Female
                                            </option>
                                            <option value="others"
                                                {{ old('gender',@$profile->gender)=='others' ? 'selected' : ''  }}>Others
                                            </option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                                            </div>
                                            <input name="dob" data-provide="datepicker"
                                            readonly
                                                value="{{old('dob',@$profile->dob)}}" data-date-autoclose="true"
                                                class="form-control" placeholder="Birth date">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="icon-globe"></i></span>
                                            </div>
                                            <input type="text" name="website"
                                                value="{{old('website',@$profile->website)}}" class="form-control"
                                                placeholder="http://">
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">

                                        <select name="country" class="form-control" id="">
                                            @if($countries)
                                                @foreach ($countries as $value)
                                                    <option
                                                    {{ old('country',@$profile->country)==$value->name ? 'selected' : ''  }}
                                                    value="{{ $value->name }}"> {{ $value->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                </div>



                            <!-- state -->
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <input type="text" name="state" class="form-control"
                                        value="{{old('state',@$profile->state)}}" placeholder="State/Province">
                                </div>
                            </div>
                            <!-- city -->
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="city"
                                        value="{{old('city',@$profile->city)}}" placeholder="City">
                                </div>
                            </div>

                            <!-- facebook -->
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="facebook"
                                        value="{{old('facebook',@$profile->facebook)}}" placeholder="Facebook">
                                </div>
                            </div>
                            <!--Twitter  -->
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="twitter"
                                        value="{{old('twitter',@$profile->twitter)}}" placeholder="Twitter">
                                </div>
                            </div>

                            <!-- Instagram -->
                            <div class="col-lg-4 col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="instagram"
                                        value="{{old('instagram',@$profile->instagram)}}" placeholder="Instagram">
                                </div>
                            </div>

                            <!-- address -->
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <textarea rows="4" type="text" name="address" class="form-control"
                                        placeholder="Address">{{old('address',@$profile->address)}}</textarea>
                                </div>
                            </div>



                    </div>
                    <button type="submit" class="btn btn-round btn-primary">Update</button> &nbsp;&nbsp;



                    </form>

                </div>
            </div>
        </div>

        <div class="col-xl-4 col-lg-4 col-md-5">
            <!-- session messages -->
            <div class="card">
                <div class="header">
                    <h2>Info</h2>
                </div>
                <div class="body">
                    <small class="text-muted">Photo: </small>

                    <div>
                        <img width="100%" height="150" style="object-fit: contain"
                            src="{{asset('public/uploads/'.@$profile->photo)}}" alt="Display picture">
                    </div>
                    <small class="text-muted">Address: </small>
                    <p> {{@$profile->address}} </p>
                    <div>
                        <iframe width="100%" height="150" frameborder="0" style="border:0" allowfullscreen
                            src="https://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=
                                    {{str_replace(',', '', str_replace(' ', '+', @$profile->address))}}&z=14&output=embed">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
