<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['auth','rbac'],'prefix'=>'profile'],function() {
    Route::get('profile', 'ProfileController@index')->name('profile.index');
    Route::PUT('profile/update/{id}', 'ProfileController@update')->name('profile.update');

});


Route::get('admin/change-password', 'ChangePasswordController@index')->name('change-password.index');

Route::put('admin/change-password/{id}', 'ChangePasswordController@update')->name('change-password.update');



Route::group(['middleware'=>['auth','rbac'],'prefix'=>'profile'],function() {
    Route::get('admin-profile/{id}', 'ProfileController@viewProfile')->name('profile.view');
});


// Frontend user profile
Route::get('user-profile', 'VisitorProfileController@index')->name('visitor.profile.view');
Route::put('user-profile/{id}', 'VisitorProfileController@update')->name('visitor.profile.post');
