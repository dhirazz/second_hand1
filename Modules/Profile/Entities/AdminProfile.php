<?php

namespace Modules\Profile\Entities;

use Illuminate\Database\Eloquent\Model;

class AdminProfile extends Model
{
    protected $fillable = [];
    protected $table = 'admin_profile';
}
