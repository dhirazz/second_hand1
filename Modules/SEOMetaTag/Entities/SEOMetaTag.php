<?php

namespace Modules\SEOMetaTag\Entities;

use Illuminate\Database\Eloquent\Model;

class SEOMetaTag extends Model
{
    protected $fillable = [];
    protected $table = 'seo_meta_tags';
}
