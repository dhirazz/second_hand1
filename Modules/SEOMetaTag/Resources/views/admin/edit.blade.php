@extends('layouts.admin.main')




@section('title')
SEO Meta Tags
@endsection
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>
                        SEO Meta Tags
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- search -->

            {{-- @include('bank::common.search') --}}
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                   
                            </a></li>
                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Edit Meta Tag
                            </a></li>
                    </ul>
                    <div class="tab-content mt-0">
                      
                        <div class="tab-pane active show" id="addUser">
                            <div class="body mt-2">

                                {!! Form::open(['route'=>[
                                    'seometatag.update',
                                    'seometatag'=>$seo_meta_tag->slug
                                    ],
                                    'method'=>'put'
                                    ])
                                !!}

                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    {{ Form::label('name','Name',[
                                                        'class'=>'input-group-text'
                                                    ]) }}
                                                </div>

                                                {!! Form::text('name',$seo_meta_tag->name,['placeholder'=>'name',
                                                'class'=>'form-control show-tick '.

                                                ($errors->has('name') ? ' is-invalid' : null )]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror



                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">

                                                    {{ Form::label('title','Title',[
                                                        'class'=>'input-group-text'
                                                    ]) }}
                                                </div>

                                                {!! Form::text('title',$seo_meta_tag->title,['placeholder'=>'Title
                                                *','class'=>'form-control show-tick '.

                                                ($errors->has('title') ? ' is-invalid' : null )]) !!}

                                            </div>
                                        </div>
                                    </div>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                    {{-- meta description --}}
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">

                                                    {{ Form::label('meta_description','Meta Description',[
                                                        'class'=>'input-group-text'
                                                    ]) }}
                                                </div>
                                                {!! Form::textarea('meta_description',$seo_meta_tag->meta_desc,[
                                                'placeholder'=>'Meta Description',
                                                'class'=>'form-control show-tick '.

                                                ($errors->has('meta_description') ? ' is-invalid' : null )
                                                ]) !!}



                                            </div>
                                        </div>
                                    </div>

                                    @error('meta_description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                    {{-- meta keyword --}}
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    {{ Form::label('meta_keyword','Meta Keyword',[
                                                            'class'=>'input-group-text'
                                                        ]) }}
                                                </div>


                                                {!! Form::textarea('meta_keyword',$seo_meta_tag->meta_keyword,[
                                                'placeholder'=>'Meta Keyword',
                                                'class'=>'form-control show-tick '.

                                                ($errors->has('meta_keyword') ? ' is-invalid' : null )
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    @error('meta_keyword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror


                                    {{-- meta auth --}}
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    {{ Form::label('auth','Auth',[
                                                            'class'=>'input-group-text'
                                                        ]) }}
                                                </div>


                                                {!! Form::text('auth',$seo_meta_tag->auth,[
                                                'class' => 'form-control'. ($errors->has('auth') ? ' is-invalid' :
                                                null )
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">

                                        {{ Form::button('Update',[
                                            'type'=>'submit',
                                            'class'=>'btn btn-primary'
                                        ]) }}

                                        <a class="btn btn-secondary"
                                            href="{{url('expensecategory/expensecategories')}}">
                                            CLOSE
                                        </a>
                                    </div>
                                </div>
                                @if($errors->any())
                                {!! implode('', $errors->all("<div class='error'>
                                    <p style='color:red'>:message</p>
                                </div>")) !!}
                                @endif
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection