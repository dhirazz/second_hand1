@extends('layouts.admin.main')

    @section('title')
        SEO Meta Tags
    @endsection

@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>
                        SEO Meta Tags
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- search -->

            {{-- @include('bank::common.search') --}}
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#Users"> SEO Meta Tags
                            </a></li>
                            @if(in_array('seometatag.store',session('permission')))

                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Add New Tag
                            </a></li>
                        @endif
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">
                            <div class="table-responsive">

                                <form method="post">
                                    @csrf
                                    @if(in_array('seometatag.delete',session('permission')))

                                    <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('seometatag.delete') }}" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                    @endif
                                    <table class="table table-hover table-custom spacing8">
                                        <thead>
                                            <tr>
                                                @if(in_array('seometatag.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick" type="checkbox" id="check_all">
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                                @endif
                                                <th>Name</th>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Auth</th>
                                                @if(in_array('seometatag.edit',session('permission')))
                                                    <th>Action</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($seo_meta_tags as $value)

                                            <tr>
                                                @if(in_array('seometatag.delete',session('permission')))

                                                <td>

                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick checkitem" type="checkbox"
                                                            name="id[]" value="{{ $value->slug }}">
                                                        <span></span>
                                                    </label>
                                                </td>
                                                @endif

                                                <td>
                                                     {{$value->name}}
                                                </td>

                                                <td>
                                                     {{$value->title}}
                                                </td>

                                                <td>
                                                     {{$value->meta_desc}}
                                                </td>


                                                <td>
                                                     {{$value->auth}}
                                                </td>
                                                @if(in_array('seometatag.edit',session('permission')))

                                                <td>
                                                    <a href="{{ route('seometatag.edit',$value->slug) }}" >
                                                            <i class="icon-pencil"></i>
                                                        </a>

                                                    </td>
                                                @endif

                                                @endforeach
                                        </tbody>
                                    </table>

                                </form>
                                {{-- {!! $categories->render() !!} --}}

                            </div>
                        </div>

                        @if(in_array('seometatag.store',session('permission')))

                        <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                            <div class="body mt-2">

                                {!! Form::open(['route'=>'seometatag.store']) !!}


                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    {{ Form::label('name','Name',[
                                                        'class'=>'input-group-text'
                                                    ]) }}
                                                </div>

                                                {!! Form::text('name',null,['placeholder'=>'name',
                                                'class'=>'form-control show-tick '.

                                                ($errors->has('name') ? ' is-invalid' : null )]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror



                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">

                                                    {{ Form::label('title','Title',[
                                                        'class'=>'input-group-text'
                                                    ]) }}
                                                </div>

                                                {!! Form::text('title',null,['placeholder'=>'Title
                                                *','class'=>'form-control show-tick '.

                                                ($errors->has('title') ? ' is-invalid' : null )]) !!}

                                            </div>
                                        </div>
                                    </div>

                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                    {{-- meta description --}}
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">

                                                    {{ Form::label('meta_description','Meta Description',[
                                                        'class'=>'input-group-text'
                                                    ]) }}
                                                </div>
                                                {!! Form::textarea('meta_description',null,[
                                                'placeholder'=>'Meta Description',
                                                'class'=>'form-control show-tick '.

                                                ($errors->has('meta_description') ? ' is-invalid' : null )
                                                ]) !!}



                                            </div>
                                        </div>
                                    </div>

                                    @error('meta_description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                    {{-- meta keyword --}}
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    {{ Form::label('meta_keyword','Meta Keyword',[
                                                            'class'=>'input-group-text'
                                                        ]) }}

                                                </div>


                                                {!! Form::textarea('meta_keyword',null,[
                                                'placeholder'=>'Meta Keyword',
                                                'class'=>'form-control show-tick '.

                                                ($errors->has('meta_keyword') ? ' is-invalid' : null )
                                                ]) !!}


                                            </div>
                                        </div>
                                    </div>

                                    @error('meta_keyword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror


                                    {{-- meta auth --}}
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    {{ Form::label('auth','Auth',[
                                                            'class'=>'input-group-text'
                                                        ]) }}



                                                </div>


                                                {!! Form::text('auth',null,[
                                                'class' => 'form-control'. ($errors->has('auth') ? ' is-invalid' :
                                                null )
                                                ]) !!}


                                            </div>
                                        </div>
                                    </div>




                                    <div class="col-12">

                                        {{ Form::button('Add',[
                                            'type'=>'submit',
                                            'class'=>'btn btn-primary'
                                        ]) }}

                                        <a class="btn btn-secondary"
                                            href="{{url('expensecategory/expensecategories')}}">
                                            CLOSE
                                        </a>
                                    </div>
                                </div>
                                @if($errors->any())
                                {!! implode('', $errors->all("<div class='error'>
                                    <p style='color:red'>:message</p>
                                </div>")) !!}
                                @endif
                                {!! Form::close() !!}
                            </div>
                        </div>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




@endsection