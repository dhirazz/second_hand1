<?php

namespace Modules\SEOMetaTag\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\SEOMetaTag\Entities\SEOMetaTag;

class SEOMetaTagController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $seo_meta_tags = SEOMetaTag::paginate(25);
        return view('seometatag::admin.index',
        compact('seo_meta_tags')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('seometatag::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'title'=>'required|unique:seo_meta_tags,title'
        ];

        $request->validate($rules);

        $seo_meta = new SEOMetaTag;
        $seo_meta->name  = $request->name;
        $seo_meta->title = $request->title;
        $seo_meta->meta_desc = $request->meta_description;
        $seo_meta->meta_keyword = $request->meta_keyword;
        $seo_meta->auth = $request->auth;
        $seo_meta->slug = $request->title;
        $seo_meta->save();

        return back();
        
        
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('seometatag::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $seo_meta_tag = SEOMetaTag::where('slug',$id)->first();

        return view('seometatag::admin.edit',
            compact('seo_meta_tag')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $seo_meta = SEOMetaTag::where('slug',$id)->first();

        $rules = [
            'title'=>'required|unique:seo_meta_tags,title,'.$seo_meta->id
        ];

        $request->validate($rules);
        
        $seo_meta->name  = $request->name;
        $seo_meta->title = $request->title;
        $seo_meta->meta_desc = $request->meta_description;
        $seo_meta->meta_keyword = $request->meta_keyword;
        $seo_meta->auth = $request->auth;
        $seo_meta->slug = $request->title;
        $seo_meta->save();

        return redirect()->route('seometatag.index');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        
    }

    public function delete(Request $request){

        
        SEOMetaTag::whereIn('slug',$request->id)->delete();

        return back();
    }
}
