<form action="{{route('subcategory.store')}}" enctype="multipart/form-data" method="post">
    @csrf
    <div class="row clearfix">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="category_name" class="input-group-text">Category</label>
                    </div>
                    <select name="category_id" id="" class="form-control">
                        <option value="">Select Main Category</option>
                        @foreach ($categories as $value)
                            <option value="{{ $value->id }}">{{ $value->category_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>

        {{-- Subcategory name --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="subcategory_name" class="input-group-text">Category</label>
                    </div>
                    <input type="text" name="subcategory_name" id="subcategory_name"
                        class="form-control show-tick @error('subcategory_name') is-invalid @enderror"
                        placeholder="Category *" value="{{old('subcategory_name')}}"
                        required="">
                </div>
            </div>
        </div>

        @error('subcategory_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror





        {{-- photo upload --}}

        <div class="col-12">
            <div class="form-group mt-3 mb-5">
                <input type="file" name="photo" class="dropify">
            </div>
        </div>


        <div class="col-12">
            <button type="submit" class="btn btn-primary">Add</button>
            <a class="btn btn-secondary"
                href="{{url('expensecategory/expensecategories')}}">
                CLOSE
            </a>
        </div>

    </div>

    @if($errors->any())
    {!! implode('', $errors->all("<div class='error'>
        <p style='color:red'>:message</p>
    </div>")) !!}
    @endif
</form>
