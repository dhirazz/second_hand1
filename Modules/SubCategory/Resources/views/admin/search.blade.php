<form data-route="{{ route('subcategory.search') }}" id="subcategory-form">

    <div class="row">

    <div class="col-md-2">
        <div class="form-group">

            <select name="category_id" id="category_id" class="form-control">
                <option value="">--Select Category--</option>
                @foreach ($categories as $value)
                    <option value="{{ $value->id }}">{{ $value->category_name }}</option>
                @endforeach
            </select>

        </div>
    </div>

    {{-- Main category --}}
    <div class="col-md-2">
        <div class="form-group">
            <select name="main_category_id" id="main_category_id" class="form-control">
                <option value="">--Select Main Category--</option>
                @foreach ($main_categories as $value)
                    <option value="{{ $value->id }}">{{ $value->main_category }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group">
            <input type="text" placeholder="Search ..." name="keyword" id="subcategory-keyword" class="form-control">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <input type="text" readonly placeholder="Date From" name="date_from"  class="form-control datepicker">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group">
            <input type="text" readonly placeholder="Date To" name="date_to"  class="form-control datepicker">
        </div>
    </div>

</div>



</form>
