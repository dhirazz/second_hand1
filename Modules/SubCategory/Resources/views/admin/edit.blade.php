@extends('layouts.admin.main')
@section('title')
Edit SubCategory
@endsection
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Edit SubCategory</h2>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active show" data-toggle="tab" href="#addUser">
                                Edit SubCategory</a>
                        </li>
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane  active show" id="addUser">
                            <div class="body mt-2">
                                <form action="{{route('subcategory.update',$subcategory->slug)}}"
                                    enctype="multipart/form-data" method="post">
                                    @csrf

                                    <input name="_method" type="hidden" value="PUT">
                                    <div class="row clearfix">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">SubCategory</span>
                                                    </div>
                                                    <input type="text" name="subcategory_name" class="form-control"
                                                        placeholder="SubCategory Name *"
                                                        value="{{old('subcategory_name',$subcategory->subcategory_name)}}"
                                                        required="">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <img src="{{ asset('public/uploads/'.$subcategory->photo) }}" height="100"
                                                alt="Image">
                                            <div class="form-group mt-3 mb-5">
                                                <input type="file" name="photo" class="dropify">
                                                <small id="fileHelp" class="form-text text-muted">This is some
                                                    placeholder block-level help text for the above input. It's a bit
                                                    lighter and easily wraps to a new line.</small>
                                            </div>
                                        </div>


                                        {{-- category --}}

                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">Category</span>
                                                    </div>



                                                    {!! Form::select('category_id', $categories,
                                                    $subcategory->category_id) !!}


                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-12">
                                            <button type="submit" class="btn btn-primary">Update</button>

                                            <a class="btn btn-secondary" href="{{url('category/categories')}}">
                                                CLOSE</a>
                                        </div>
                                    </div>

                                    @if($errors->any())
                                        {!! implode('', $errors->all("<div class='error'><p style='color:red'>:message</p></div>")) !!}
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection