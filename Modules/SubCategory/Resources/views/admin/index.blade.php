@extends('layouts.admin.main')
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>
                        SubCategories
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">

            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#Users"> SubCategories
                            </a></li>
                            @if(in_array('category.store',session('permission')))

                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Add New SubCategory
                            </a></li>
                            @endif
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">

                            {{-- Search --}}
                                @include('subcategory::admin.search')

                            <div class="table-responsive">

                                <form method="post">
                                    @csrf
                                    @if(in_array('subcategory.delete',session('permission')))

                                    <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('subcategory.delete') }}" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>

                                    @endif
                                    <table class="table table-hover table-custom spacing8"
                                    id="table-subcategory">
                                        <thead>
                                            <tr>
                                                @if(in_array('subcategory.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick" type="checkbox" id="check_all">
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                                @endif
                                                <th>Sub Category</th>
                                                <th>Category</th>
                                                <th>Main Category</th>

                                                @if(in_array('subcategory.edit',session('permission')))

                                                    <th>Actions</th>
                                                @endif
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach ($subcategories as $value)

                                            <tr>
                                                @if(in_array('subcategory.delete',session('permission')))

                                                    <td>

                                                        <label class="fancy-checkbox">
                                                            <input class="checkbox-tick checkitem" type="checkbox"
                                                                name="slug[]" value="{{ $value->slug }}">
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                @endif
                                                <td>{{ $value->subcategory_name }}</td>

                                                <td>
                                                    <h6 class="mb-0"> {{$value->category_name}}</h6>
                                                </td>

                                                <td>
                                                    <h6 class="mb-0"> {{$value->main_category}}</h6>
                                                </td>

                                                @if(in_array('subcategory.edit',session('permission')))


                                                    <td>
                                                        <a href="{{route('subcategory.edit',$value->slug)}}">
                                                            <i class="icon-pencil"></i>
                                                        </a>

                                                    </td>

                                                @endif

                                                @endforeach
                                        </tbody>
                                    </table>
                                </form>
                                {{-- {!! $categories->render() !!} --}}

                            </div>
                        </div>

                        @if(in_array('category.store',session('permission')))

                        <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                            <div class="body mt-2">

                                @include('subcategory::admin.create')
                            </div>
                        </div>

                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

