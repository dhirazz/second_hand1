<?php

namespace Modules\SubCategory\Entities;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [];
    protected $table="categories";
}
