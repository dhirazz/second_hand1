<?php

namespace Modules\SubCategory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\SubCategory\Entities\SubCategory;
use Modules\SubCategory\Entities\Category;

use File;
use Modules\MainCategory\Entities\MainCategory;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $subcategories = SubCategory::select('*', 'sub_categories.slug as slug')
            ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
            ->join('main_categories', 'categories.main_category_id', '=', 'main_categories.id')
            ->orderBy('subcategory_name')
            ->paginate(25);

        $categories = Category::orderBy('category_name')->get();

        $main_categories = MainCategory::orderBy('main_category')->get();

        return view('subcategory::admin.index',
            compact('subcategories', 'categories','main_categories')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('subcategory::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'subcategory_name' => 'required|unique:sub_categories,subcategory_name',
            'category_id' => 'required'
        ];

        $field_names = [
            'category_id' => ' Category'
        ];


        $request->validate($rules,[],$field_names);

        $subcategory = new SubCategory;
        $subcategory->subcategory_name = $request->subcategory_name;
        $subcategory->category_id = $request->category_id;
        $subcategory->slug = $request->subcategory_name;


        if ($request->hasFile('photo')) {
            $file = $request->file('photo');
            $photo = time() . '.' . $file->getClientOriginalExtension();
            $destination_path = public_path('/uploads');
            $file->move($destination_path, $photo);

            $subcategory->photo = $photo;
        }


        $subcategory->save();
        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('subcategory::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $subcategory = SubCategory::where('slug', $id)->first();
        $categories = Category::all();

        $categories = $categories->pluck('category_name', 'id')->toArray();

        return view(
            'subcategory::admin.edit',
            compact('subcategory', 'categories')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {

        $subcategory = SubCategory::where('slug', $id)->first();

        $rules = [
            'subcategory_name' => 'required|unique:sub_categories,subcategory_name,'.$subcategory->id,
            'category_id' => 'required'
        ];

        $field_names = [
            'category_id' => ' Category'
        ];


        $request->validate($rules,[],$field_names);

        $subcategory->subcategory_name = $request->subcategory_name;
        $subcategory->category_id = $request->category_id;
        $subcategory->slug = $request->subcategory_name;

        if ($request->hasFile('photo')) {

            //remove file from the folder

            $old_file = public_path('/uploads') . '/' . $subcategory->photo;

            if (File::exists($old_file)) {
                File::delete($old_file);
            }

            //upload new one
            $file = $request->file('photo');
            $photo = time() . '.' . $file->getClientOriginalExtension();
            $destination_path = public_path('/uploads');
            $file->move($destination_path, $photo);

            $subcategory->photo = $photo;
        }
        $subcategory->save();

        return redirect()->route('subcategory.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    // public function destroy($id)
    // {
    //     //
    // }

    public function delete(Request $request)
    {

        if ($request->slug) {
            $subcategory = SubCategory::whereIn('slug', $request->slug)->delete();
        }

        return back();
    }

    // Search

    public function search(Request $request){

        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $subcategories = SubCategory::select('*', 'sub_categories.slug as slug')
        ->join('categories', 'sub_categories.category_id', '=', 'categories.id')
        ->join('main_categories', 'categories.main_category_id', '=', 'main_categories.id');

        if($request->keyword){

            $subcategories = $subcategories->where('subcategory_name', 'like', '%' .$request->keyword . '%');

        }

        if($request->category_id){
            $subcategories = $subcategories->where('category_id',$request->category_id);
        }

        if($request->main_category_id){
            $subcategories = $subcategories->where('main_category_id',$request->main_category_id);
        }

        if($request->date_from && $request->date_to){

            $date_from = date('Y-m-d',strtotime($date_from));
            $date_to = date('Y-m-d',strtotime($date_to));

            $subcategories = $subcategories->whereBetween('sub_categories.created_at',[$date_from,$date_to]);
        }

        $subcategories = $subcategories->orderBy('subcategory_name')->get();

        return response()->json($subcategories);
    }
}
