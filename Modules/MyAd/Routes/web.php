<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('myad')->group(function() {
    Route::resource('myad', 'MyAdController');
    Route::post('delete', 'MyAdController@delete')->name('myad.delete');
    Route::get('delete-photo/{id}', 'MyAdController@deletePhoto')->name('myad.delete.photo');

});
