<?php

namespace Modules\MyAd\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\MyAd\Entities\MyAd;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductPhoto;
use Modules\SubCategory\Entities\SubCategory;
use File;
use Modules\Category\Entities\Category;
use PhpParser\Node\Stmt\Foreach_;

class MyAdController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $myads = MyAd::select('*', 'products.id as id')
            ->join('products', 'my_ads.ad_id', '=', 'products.id')
            ->join('visitors', 'my_ads.visitor_id', '=', 'visitors.id')
            ->where('visitors.id', auth()->guard('visitor')->user()->id)
            ->get();

        // dd($myads);

        return view(
            'myad::front.index',
            compact('myads')
        )->with(['controller' => $this]);;
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $categories = Category::orderBy('category_name')->get();
        return view('myad::front.create',
            compact('categories')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('myad::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $product = Product::where('slug', $id)->first();
        $subcategories = SubCategory::orderBy('subcategory_name')->get();

        return view(
            'myad::front.edit',
            compact('product', 'subcategories')
        )->with(['controller' => $this]);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $product = Product::where('slug', $id)->first();

        $rules = [
            'title' => 'required',
            'subcategory_id' => 'required',
            'price' => 'required',
            'email' => 'required|email',
            'ad_number' => 'required',
            'photo.*' => 'mimes:jpg,jpeg,gif,png',
            'video' => 'mimes:flv,mp4,3gp,wmv,dat,avi,mov,mkv,ogg'
        ];

        $names = [
            'subcaetgory_id' => 'Sub Category'

        ];

        $request->validate($rules, [], $names);

        $product->product_name = $request->product_name;
        $product->title = $request->title;
        $product->subcategory_id = $request->subcategory_id;
        $product->price    = $request->price;
        $product->condition    = $request->condition;
        $product->mississippi = $request->mississippi;
        $product->contact_name    = $request->contact_name;
        $product->email    = $request->email;
        $product->ad_number    = $request->ad_number;
        $product->website    = $request->website;
        $product->location = $request->location;


        // Video upload
        // Upload the video

        if ($request->has('video')) {
            //old video
            $old_video = public_path('videos/') . $product->video;

            //  dd($old_video);

            if (File::exists($old_video)) {
                File::delete($old_video);
            }

            $video = $request->file('video');
            $time = date('YmdHis');
            $videoName = $time . '-' . $video->getClientOriginalName();
            $video->move(public_path() . '/videos/', $videoName);
            $product->video = $videoName;
        }

        $product->save();

        //if photo is selected upload and save photos

        if ($request->has('photo')) {

            //remove all old photos
            $product_photos = ProductPhoto::where('product_id',$product->id)->get();

            foreach ($product_photos as $value) {
                $value->featured_image = '0';
                $value->save();
            }

            $i =0;
            foreach ($request->file('photo') as $item) {
                $product_photo = new ProductPhoto();
                $time = date('YmdHis');
                $imageName = $time . '-' . $item->getClientOriginalName();
                $item->move(public_path() . '/product/', $imageName);
                $product_photo->photo = $imageName;
                $product_photo->product_id = $product->id;

                if(@$request->featured_image[$i]){
                    $product_photo->featured_image = 1;
                }
                $product_photo->save();

                $i++;
            }
        }


        return redirect()->route('myad.index')->with('msg','Ad Information Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete(Request $request)
    {
        $id = $request->id;

        $product = Product::find($id);

        // get product photo

        $product_photos = ProductPhoto::where('product_id', $id)->get();

        $path = public_path('product') . '/';

        foreach ($product_photos as $value) {
            if (File::exists($path . $value->photo)) {
                File::delete($path . $value->photo);
            }
        }

        $product->delete();

        return back();
    }

    // Delete photo

    public function deletePhoto($id){

        $product_photo = ProductPhoto::find($id);

        $path = public_path('product') . '/';

        if (File::exists($path . $product_photo->photo)) {
            File::delete($path . $product_photo->photo);
        }


        $product_photo->delete();

        return back();

    }


    //get product photos
    public function getProductPhotos($id)
    {
        $photos = ProductPhoto::where('product_id', $id)->get();
        return $photos;
    }
}
