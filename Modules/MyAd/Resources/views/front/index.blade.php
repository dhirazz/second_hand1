@section('title')
User Profile
@endsection


@extends('layouts.front.main')

@section('content')

<div class="product-area section">
    <div class="container">

        <div class="row clearfix">


            <div class="col-xl-8 col-lg-8 col-md-7">
                <div class="card">
                    <div class="header">
                        <h2>Manage Ads</h2>
                    </div>
                    <div class="body">

                        @if(Session::has('message'))
                            <p class="alert alert-info">{{ Session::get('message') }}</p>
                        @endif


                        <div class="row">
                            <div class="col-lg-6 col-md-12">
                                <table>

                                        @foreach ($myads as $value)
                                        <tr>
                                            <th>Title</th>
                                            <td>{{ $value->title }}</td>
                                        </tr>

                                        <tr>
                                            <th>Photos</th>
                                            <td>
                                                @php
                                                    $photos = $controller->getProductPhotos($value->ad_id);
                                                @endphp

                                                <table>
                                                        @foreach ($photos as $v)
                                                            <td>
                                                                <img src="{{ asset('public/product/'.$v->photo) }}" alt="">
                                                            </td>
                                                        @endforeach
                                                </table>
                                            </td>
                                        </tr>

                                        <tr>
                                            <th>Actions</th>
                                            <td>
                                                <a href="{{ route('myad.edit',$value->slug) }}">Edit</a>
                                                |
                                                <a href="{{ route('myad.delete') }}">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-5">
                <!-- session messages -->
                <div class="card">
                    <div class="header">
                        <h2>Manage Ads</h2>
                    </div>
                    <div class="body">

                        <ul>
                            <li>
                                <a href="{{ route('myad.index') }}"> All Ads </a>
                            </li>
                            <li>
                                Expired Ads

                            </li>
                            <li>
                                <a href="{{ route('visitor.profile.view') }}"> Profile </a>

                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
