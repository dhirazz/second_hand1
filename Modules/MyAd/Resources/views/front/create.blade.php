@extends('layouts.front.main')

@section('title')
Post An Add
@endsection

@section('content')

<!-- Start Contact -->
<section id="contact-us" class="contact-us section">
    <div class="container">

        <form action="{{route('myad.store')}}" enctype="multipart/form-data" method="post">
            @csrf

            <div class="contact-head">
                <div class="row">

                    <div class="form-main">
                        <div class="title">
                            <h4>Post An Add</h4>
                        </div>

                        <div class="row">
                              {{-- Product Name --}}
                              <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label for="product_name" class="input-group-text">Product Name</label>

                                        <input type="text" name="product_name" id="product_name"
                                            class="form-control show-tick @error('product_name') is-invalid @enderror"
                                            placeholder="Product Name *" value="{{old('product_name')}}" required="">
                                    </div>
                                </div>



                            {{-- title --}}
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="title" class="input-group-text">Title</label>
                                    <input type="text" name="title" id="title"
                                        class="form-control show-tick @error('title') is-invalid @enderror"
                                        placeholder="Title *" value="{{old('title')}}" required="">
                                </div>
                            </div>

                            {{-- Categories --}}
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="category_id" class="input-group-text">Category</label>
                                    <select name="category_id" class="form-control" id="category_id"
                                        class="form-control">
                                        <option value="">Select Category</option>
                                        @foreach ($categories as $value)
                                            <option value="{{ $value->id }}">
                                                {{ $value->category_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            {{-- sub categories --}}
                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="subcategory_id" class="input-group-text">Sub
                                        Category</label>
                                    <select name="subcategory_id" class="form-control" id="subcategory_id"
                                        class="form-control">
                                        <option value="">Select SubCategory</option>
                                    </select>
                                </div>
                            </div>


                            {{-- Price --}}
                            <div class="col-lg-6 col-12">
                                <div class="form-group">

                                    <label for="price" class="input-group-text">Price</label>

                                    <input type="text" name="price" id="price"
                                        class="form-control show-tick @error('price') is-invalid @enderror"
                                        placeholder="Price *" value="{{old('price')}}" required="">
                                </div>
                            </div>



                            {{-- Condition --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">

                                    <label>Condition</label> &nbsp;&nbsp;

                                    <label class="fancy-radio custom-color-green"><input name="condition" value="New"
                                            type="radio"><span><i></i>New</span></label>
                                    <label class="fancy-radio custom-color-green"><input name="condition" value="Used"
                                            type="radio" checked><span><i></i>Used</span></label>
                                </div>
                            </div>




                            {{-- Mississippi --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">

                                    <label>Mississippi</label> &nbsp;&nbsp;

                                    <label class="fancy-radio custom-color-green">
                                        <input name="mississippi" value="Mr" type="radio" checked><span><i></i>Mr</span>
                                    </label>
                                    <label class="fancy-radio custom-color-green">
                                        <input name="mississippi" value="Mrs" type="radio"><span><i></i>Mrs</span>
                                    </label>
                                    <label class="fancy-radio custom-color-green">
                                        <input name="mississippi" value="Ms" type="radio"><span><i></i>Ms</span>
                                    </label>
                                    <label class="fancy-radio custom-color-green">
                                        <input name="mississippi" value="Other" type="radio"><span><i></i>Other</span>
                                    </label>
                                </div>
                            </div>

                            {{-- Contact Name --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="contact_name" class="input-group-text">Contact
                                        Name</label>
                                    <input type="text" name="contact_name" id="contact_name"
                                        class="form-control show-tick @error('price') is-invalid @enderror"
                                        placeholder="Contact Name *" value="{{old('contact_name')}}" required="">
                                </div>
                            </div>





                            {{-- email --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="email" class="input-group-text">Email</label>
                                    <input type="email" name="email" id="email"
                                        class="form-control show-tick @error('email') is-invalid @enderror"
                                        placeholder="Email *" value="{{old('email')}}" required="">
                                </div>
                            </div>


                            {{-- Advertisement number --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="ad_number" class="input-group-text">Ad
                                        Number</label>
                                    <input type="text" name="ad_number" id="ad_number"
                                        class="form-control show-tick @error('ad_number') is-invalid @enderror"
                                        placeholder="Ad Number *" value="{{old('ad_number')}}" required="">
                                </div>
                            </div>



                            {{-- Website --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="website" class="input-group-text">Website</label>
                                    <input type="text" name="website" id="website"
                                        class="form-control show-tick @error('website') is-invalid @enderror"
                                        placeholder="Website " value="{{old('website')}}">
                                </div>
                            </div>



                            {{-- Location --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <label for="location" class="input-group-text">Location</label>
                                    <input type="text" name="location" id="location"
                                        class="form-control show-tick @error('location') is-invalid @enderror"
                                        placeholder="Location " value="{{old('location')}}" required="">
                                </div>
                            </div>






                            {{-- photo upload --}}


                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <input type="file" name="photo[]" multiple id="photo" class="dropify"
                                        onchange="preview_image();">

                                    <small id="fileHelp" class="form-text text-muted">to select multiple
                                        files, hold down the CTRL or SHIFT key while selecting..</small>

                                        <div id="image_preview"></div>
                                    </div>
                            </div>

                            {{-- Preview image --}}



                            {{-- Submit button --}}

                            <div class="col-lg-6 col-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Post Ad</button>
                                </div>
                            </div>
                        </div>




        </form>

        @if($errors->any())
        {!! implode('', $errors->all("<div class='error'>
            <p style='color:red'>:message</p>
        </div>")) !!}
        @endif


    </div>
</section>
<!--/ End Contact -->
@endsection
