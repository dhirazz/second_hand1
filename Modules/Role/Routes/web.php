<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>['auth','rbac'],'preifix'=>'role'],function() {
    Route::resource('role','RoleController');
    Route::post('role/delete','RoleController@delete')->name('role.delete');

});
