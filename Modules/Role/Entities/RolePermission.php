<?php

namespace Modules\Role\Entities;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{
    protected $fillable = [];
    protected $table ="role_permissions";
}
