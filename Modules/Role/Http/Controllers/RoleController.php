<?php

namespace Modules\Role\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Role\Entities\Permission;
use Modules\Role\Entities\Role;
use Modules\Role\Entities\RolePermission;

use Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $roles = Role::where('type', 'regular')->orderBy('name','ASC')->paginate(config('number_of_pages'));

        $permissions = Permission::groupby('module')->get();


        return view('role::admin.index',
            compact('roles', 'permissions')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('role::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {

        $rules = [
            'name' => 'required|unique:roles',
            // 'permission_id' => 'required'
        ];

        $request->validate($rules);


        $role = new Role;

        $role->name = $request->name;

        $role->role_code = md5($role->name).''.date('ymdhis').''.rand(0,9999999);


        //created_by
        $user_id = auth()->user()->id;
        $role->save();


        if ($request->permission_id) {
            //assign the permission to created role
            foreach ($request->permission_id as $value) {

                $role_permission = new RolePermission;
                $role_permission->role_id = $role->id;
                $role_permission->permission_id = $value;

                $role_permission->save();
            }
        }




        // for additional settings

        // if ($request->settings) {

        //     $role_permission = new RolePermission;
        //     $role_permission->role_id = $role->id;
        //     $role_permission->permission_id = $request->settings;

        //     $role_permission->save();

        //     //for post settings
        //     $role_permission = new RolePermission;
        //     $role_permission->role_id = $role->id;
        //     $role_permission->permission_id = $request->post_settings;
        //     $role_permission->save();
        // }


        Session::flash('message', 'Role Created Successfully!');

        Session::flash('alert-class', 'alert-success');


        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('role::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $role = Role::where('role_code',$id)->first();


        $permissions = Permission::groupby('module')->get();

        $role_permission = RolePermission::where('role_id', $id)
            ->pluck('permission_id')
            ->all();

        return view('role::admin.edit',
            compact('role', 'permissions', 'role_permission')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $role = Role::where('role_code',$id)->first();

        $id= $role->id;

        $rules = [
            'name' => 'required|unique:roles,name,'.$id,
            // 'type' =>'required'
        ];


        $request->validate($rules);



        //delete all previously assigned permission

        $role_permission = RolePermission::where('role_id', $id)->get();

        foreach ($role_permission as $value) {
            $value->delete();
        }


        $role->name = $request->name;
        $role->save();

        // now re-assign permission from the selected form
        if ($request->permission_id) {
            foreach ($request->permission_id as $value) {
                $role_permission = new RolePermission();
                $role_permission->role_id = $id;
                $role_permission->permission_id = $value;
                $role_permission->save();
            }
        }

        // for additional settings

        // if ($request->settings) {
        //     $role_permission = new RolePermission;
        //     $role_permission->role_id = $role->id;
        //     $role_permission->permission_id = $request->settings;

        //     $role_permission->save();

        //     //for post settings
        //     $role_permission = new RolePermission;
        //     $role_permission->role_id = $role->id;
        //     $role_permission->permission_id = $request->post_settings;
        //     $role_permission->save();
        // }


        Session::flash('message', 'Role Updated Successfully!');

        Session::flash('alert-class', 'alert-success');

        return redirect()->route('role.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {


        if ($request->id) {
            $role = Role::whereIn('id', $request->id)->delete();
        }

        return back();
    }
}
