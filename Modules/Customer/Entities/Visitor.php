<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = [];

    protected $table="visitors";
}
