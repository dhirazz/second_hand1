<?php

namespace Modules\Customer\Http\Controllers;

use App\Visitor;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $visitors = Visitor::orderBy('name')->get();
        return view('customer::admin.index',
            compact('visitors')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('customer::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required', 'string', 'max:255', 'min:2'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'username' => ['required', 'string', 'max:255', 'unique:users,username'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'mobile' => 'required|numeric|digits:10|unique:users,mobile'
        ];

        $request->validate($rules);


        $user = new Visitor;
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);

        // $user->profile_code = md5($user->password).date('ymdhis');

        $user->save();

        return back();
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('customer::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $customer = Visitor::find($id);
        return view('customer::admin.edit',
            compact('customer')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'required|min:2',
            'email' => 'required|email|unique:users,email,'.$id,
            'username' => 'required|unique:users,username,'.$id,
            'mobile' => 'required|numeric|digits:10|unique:users,mobile,'.$id
        ];

        $request->validate($rules);


        $user = Visitor::find($id);
        $user->name = $request->name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);

        // $user->profile_code = md5($user->password).date('ymdhis');

        $user->save();

        return redirect()->route('customer.index');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function delete(Request $request)
    {

    }
}
