@extends('layouts.admin.main')

    @section('title')
        Customers
    @endsection

@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Customer List</h2>
                </div>
            </div>
        </div>
        <div class="row clearfix">

            <!-- search -->

            {{-- @include('user::search.user-search') --}}

            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif" data-toggle="tab" href="#Users">Customers</a></li>

                        @if(in_array('user.store',session('permission')))

                            <li class="nav-item">
                                <a class="nav-link @if(session()->get('errors')) active show @endif" data-toggle="tab" href="#addUser">
                                    Add Customer</a></li>
                        @endif

                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane  @if(!session()->get('errors')) active show @endif" id="Users">
                           {{-- Search --}}

                           @include('customer::admin.search')

                           
                            <div class="table-responsive">
                                <form method="post">
                                    @csrf
                                    @if(in_array('customer.delete',session('permission')))
                                        <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('customer.delete') }}" type="submit">
                                        <i class="fa fa-trash"></i>
                                        </button>
                                    @endif
                                <table class="table table-hover table-custom spacing8">
                                    <thead>
                                        <tr>
                                            @if(in_array('customer.delete',session('permission')))

                                                <th>
                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick" type="checkbox" id="check_all"
                                                        >
                                                        <span>#</span>
                                                    </label>
                                                </th>
                                            @endif
                                            <th class="w60">Name</th>
                                            <th></th>
                                            <th>Email</th>
                                            <th>Username</th>
                                            <th>Created Date</th>

                                            @if(in_array('customer.edit',session('permission')))
                                                <th>Action</th>

                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($visitors as $value)

                                        <tr>
                                            @if(in_array('customer.delete',session('permission')))
                                            <td>

                                                <label class="fancy-checkbox">
                                                    <input class="checkbox-tick checkitem"
                                                   type="checkbox"
                                                   name="id[]" value="{{ $value->id }}">
                                                   <span></span>
                                               </label>
                                            </td>
                                            @endif
                                            <td class="width45">
                                                <div class="avtar-pic w35 bg-pink" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{$value->first_name.' '.$value->last_name}}">
                                                    @if($value->photo)

                                                    <img src="{{asset('public/uploads/'.$value->photo)}}" alt="" height="35px" width="35px">
                                                    @else
                                                    <span>
                                                        <img height="35px" width="35px" src="{{asset('public/uploads/avatar.webp')}}" alt="">

                                                    </span>
                                                    @endif
                                                </div>
                                            </td>
                                            <td>
                                                <h6 class="mb-0">{{$value->name}}</h6>
                                                <span>{{$value->email}}</span>
                                            </td>

                                            <td>{{$value->email}}</td>


                                            <td> {{$value->username}} </td>


                                            <td>{{date('d M, Y',strtotime($value->created_at))}}</td>

                                            @if(in_array('customer.edit',session('permission')))

                                                <td>
                                                    <a href="{{route('customer.edit',$value->id)}}"> <i class="icon-pencil"></i> </a>
                                                    </a>
                                                </td>

                                            @endif
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>
                            </form>

                                {{-- {!! $visitors->render() !!} --}}
                            </div>
                        </div>

                        @if(in_array('customer.store',session('permission')))

                            <div class="tab-pane @if(session()->get('errors'))  active show  @endif" id="addUser">
                                <div class="body mt-2">

                                    @include('customer::admin.create')
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
