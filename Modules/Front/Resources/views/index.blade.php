@extends('layouts.front.main')

@section('content')

<div class="slider">
  <div id="slider-animation" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
      <li data-target="#slider-animation" data-slide-to="0" class="active"></li>
      <li data-target="#slider-animation" data-slide-to="1"></li>
      <li data-target="#slider-animation" data-slide-to="2"></li>
      <li data-target="#slider-animation" data-slide-to="3"></li>
    </ul>

    <!-- The slideshow -->
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="{{asset('public/front/./images/banner1.jpg')}}" alt="">
      </div>
      <div class="carousel-item">
        <img src="{{asset('public/front/./images/banner2.jpg')}}" alt="">
      </div>
      <div class="carousel-item">
        <img src="{{asset('public/front/./images/banner3.jpg')}}" alt="">
      </div>    
      <div class="carousel-item">
        <img src="{{asset('public/front/./images/banner4.jpg')}}" alt="">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="{{asset('#slider-animation')}}" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="{{asset('#slider-animation')}}" data-slide="next">
      <span class="carousel-control-next-icon"></span>
    </a>

  </div>
</div>
<div class="search-items">
  <div class="container">
    <div class="row catebox">
      <div class="col-md-3 p-0">
        <div class="input-group">
          <select>
            <option><a href="#"><i class="far fa-hand-point-right"></i> All Categories</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Women's Clothing</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Cellphones & Telecommunications</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Computer & Office</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Consumer Electronics</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Jewelry & Accessories</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Home & Garden</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Luggage & Bags</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Shoes</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Mother & Kids</a></option>
            <option><a href="#"><i class="far fa-hand-point-right"></i> Sports & Entertainment</a></option>
          </select>
        </div>
      </div>
      <div class="col-md-4 p-0">
        <div class="input-group">        
          <input type="text" class="form-control" name="x" placeholder="I am liikong for...">
        </div>
      </div>
      <div class="col-md-3 p-0">
        <div class="input-group">
          <input type="text" class="form-control" name="x" placeholder="I am liikong for...">
        </div>
      </div>
      <div class="col-md-1 p-0">
        <div class="input-group">
          <select>
            <option>+0km</option>
            <option>+2km</option>
            <option>+5km</option>
            <option>+10km</option>
            <option>+20km</option>
            <option>+50km</option>
            <option>+100km</option>
          </select>
        </div>
      </div>
      <div class="col-md-1 search-category p-0">
        <div class="input-group">
          <span class="input-group-btn">
              <button class="btn btn-default" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="item-list">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <ul>
          <li><a href="#"><i class="fas fa-mobile-alt"></i> <h6>Electronics</h6></a></li>
          <li><a href="#"><i class="fas fa-car-alt"></i> <h6>cars</h6></a></li>
          <li><a href="#"><i class="fas fa-volleyball-ball"></i> <h6>Sports</h6></a></li>
          <li><a href="#"><i class="fas fa-motorcycle"></i> <h6>Motorcycle</h6></a></li>
          <li><a href="#"><i class="fas fa-dumbbell"></i> <h6>Gym</h6></a></li>
          <li><a href="#"><i class="fas fa-headphones-alt"></i> <h6>Movies</h6></a></li>
          <li><a href="#"><i class="fas fa-shoe-prints"></i> <h6>Fashion</h6></a></li>
          <li><a href="#"><i class="fas fa-gift"></i> <h6>Gifts</h6></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="flash-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h3>Flash Sell</h3>
      </div>
      <div class="col-12 col-sm-12 flash-item owl-carousel">
        <div class="testi-box">
          <img src="{{asset('public/front/./images/add-new1.JPG')}}" alt="">
          <h5><a href="#">Product Title Here</a></h5>
          <p>Rs. 1,20,000</p>
          <h6>(Used)</h6>
        </div>
        <div class="testi-box">
          <img src="{{asset('public/front/./images/add-new2.JPG')}}" alt="">
          <h5><a href="#">Product Title Here</a></h5>
          <p>Rs. 1,20,000</p>
          <h6>(Used)</h6>
        </div>
        <div class="testi-box">
          <img src="{{asset('public/front/./images/add-new3.JPG')}}" alt="">
          <h5><a href="#">Product Title Here</a></h5>
          <p>Rs. 1,20,000</p>
          <h6>(Used)</h6>
        </div>
        <div class="testi-box">
          <img src="{{asset('public/front/./images/add-new1.JPG')}}" alt="">
          <h5><a href="#">Product Title Here</a></h5>
          <p>Rs. 1,20,000</p>
          <h6>(Used)</h6>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="deal-section">
  <div class="container">
      <div class="row">
        <div class="col-12 col-sm-12">
          <h4>Daily Deals</h4>
        </div>
          <div class="col-12 col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#home">Men</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#menu1">Women</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#fashion">Fashions</a>
              </li>              
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#sport">Sports</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#electronics">Electronics</a>
              </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div id="home" class="tab-pane active">
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <ul>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product1.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product2.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product3.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product4.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product5.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="menu1" class="tab-pane fade"><br>
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <ul>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product1.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product2.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product3.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product4.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product5.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="fashion" class="tab-pane fade"><br>
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <ul>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product1.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product2.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product3.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product4.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product5.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="sport" class="tab-pane fade"><br>
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <ul>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product1.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product2.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product3.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product4.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product5.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div id="electronics" class="tab-pane fade"><br>
                <div class="row">
                  <div class="col-12 col-sm-12">
                    <ul>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product1.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product2.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product3.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product4.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="deal-box">
                          <figure>
                            <a href="#"><img src="./images/product5.jpg" alt=""></a>
                          </figure>
                          <div class="pro-detail">
                            <h5><a href="#">Product Title Here</a></h5>
                            <p>Rs. 1,20,000</p>
                            <h6>(Used)</h6>
                          </div>
                        </div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
  </div>
</div>
<div class="feature-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Feature Products</h4>
      </div>
      <div class="col-12 col-sm-12">
        <ul>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product1.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product2.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product3.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product4.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product5.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product6.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product1.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product2.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product3.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product4.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="best-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Best Deals</h4>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product1.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product2.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product3.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product4.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product5.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product6.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product1.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product2.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product3.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product4.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product5.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product6.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product1.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product2.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product3.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product4.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product5.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-6 col-md-3 col-lg-2">
        <div class="deal-box1">
          <figure>
            <a href="#"><img src="./images/product6.jpg" alt=""></a>
          </figure>
          <div class="pro-detail">
            <h5><a href="#">Product Title Here</a></h5>
            <p>Rs. 1,20,000</p>
            <h6>(Used)</h6>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="search-section">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-12">
        <h4>Recent Searches</h4>
      </div>
      <div class="col-12 col-sm-12">
        <ul>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product1.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product2.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product3.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product4.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product5.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product6.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product1.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product2.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product3.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
          <li>
            <div class="deal-box">
              <figure>
                <a href="#"><img src="./images/product4.jpg" alt=""></a>
              </figure>
              <div class="pro-detail">
                <h5><a href="#">Product Title Here</a></h5>
                <p>Rs. 1,20,000</p>
                <h6>(Used)</h6>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>



<script>
  $(window).scroll(function() {    
var scroll = $(window).scrollTop();

if (scroll >= 200) {
    $(".logo-section").addClass("fixed-top");
} else {
    $(".logo-section").removeClass("fixed-top");
}
}); 
</script>

@endsection