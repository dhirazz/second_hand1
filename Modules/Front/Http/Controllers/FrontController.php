<?php

namespace Modules\Front\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
// use Modules\Category\Entities\Category;
// use Modules\Category\Entities\MainCategory;
use Location;
// use Modules\Product\Entities\Product;

class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $ip = request()->ip();

        $ip = '27.34.21.50';

        // $data = Location::get($ip);

        // $suggested_products = Product::where(function($q) use ($data){
        //     $q->where('city',$data->cityName)
        //       ->orWhere('state', $data->regionName)
        //       ->orWhere('country', $data->countryName);
        // })->get();

        // dd($suggested_products);
        $categories = DB::table('Categories')

            // ->join('main_categories', 'id', 'categories.main_categories_id')

            ->get();
        dd($categories);

        return view(
            'front::index',
            compact('categories')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('front::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('front::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('front::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
