<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('product')->group(function() {
    Route::resource('product','ProductController');
    Route::post('product/delete','ProductController@delete')->name('product.delete');

});
Route::get('product-photo/{id}','ProductController@deletePhoto')->name('delete.photo');


// Front end
Route::get('product/post-add','FrontProductController@create')->name('product.front.create');
Route::post('product/post-add','FrontProductController@store')->name('product.front.post');


// Get subcategory
// Route::get('product/get-subcategory/{id}','FrontProductController@getSubcategory')->name('product.getsubcategory');




// GEt category
Route::get('product/get-category/{id}','ProductController@getCategory')->name('product.getcategory');

// Get subcategory
Route::get('product/get-subcategory/{id}','ProductController@getSubcategory')->name('product.getsubcategory');
