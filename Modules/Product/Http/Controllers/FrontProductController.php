<?php

namespace Modules\Product\Http\Controllers;

use App\Setting;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductPhoto;
use Modules\SubCategory\Entities\SubCategory;

use Location;
use Modules\Product\Entities\MyAd;

class FrontProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        return view('product::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $setting = Setting::first();
        $subcategories = SubCategory::orderBy('subcategory_name')->get();
        return view('product::front.create',
        compact('setting','subcategories')
        );
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'subcategory_id' => 'required',
            'price' => 'required',
            'email' => 'required|email',
            'ad_number' => 'required',
            'photo.*' => 'image'
        ];

        $names = [
            'subcategory_id' => 'Sub Category'

        ];



        $request->validate($rules, [], $names);


        //fetch the ip address

        $ip = request()->ip;


        $ip = '27.34.21.50' ;

        // dd($ip);

        $data = Location::get($ip);

        //dd($data);

        $customer_data = [
            $data->cityName,
            $data->regionName,
            $data->countryName
        ];

        // dd


        $product = new Product;
        $product->product_name = $request->product_name;

        $product->title = $request->title;
        $product->subcategory_id = $request->subcategory_id;
        $product->price = $request->price;
        $product->condition = $request->condition;
        $product->mississippi = $request->mississippi;
        $product->contact_name = $request->contact_name;
        $product->email = $request->email;
        $product->ad_number = $request->ad_number;
        $product->website = $request->website;
        $product->location = $request->location;
        $product->slug = md5($product->title) . date('ymdhis') . rand(0, 9999);

        // fetch product information from the ip
        $product->city = $data->cityName;

        $product->state = $data->regionName;

        $product->country = $data->countryName;
        $product->save();

        //if photo is selected upload and save photos


        if ($request->has('photo')) {

            foreach ($request->file('photo') as $item) {
                $product_photo = new ProductPhoto();
                $time = date('YmdHis');
                $imageName = $time . '-' . $item->getClientOriginalName();
                //$path =
                $item->move(public_path() . '/product/', $imageName);
                $product_photo->photo = $imageName;
                $product_photo->product_id = $product->id;
                $product_photo->save();
            }
        }


        // add product to myads

        $myad = new MyAd();
        $myad->ad_id = $product->id;
        $myad->visitor_id = auth()->guard('visitor')->user()->id;
        $myad->save();

        return redirect()->route('front.page');
    }


    // Get subcategory

    public function getSubcategory($id){
        $subcategories = SubCategory::where('category_id',$id)->get();
        return response()->json();
    }
}
