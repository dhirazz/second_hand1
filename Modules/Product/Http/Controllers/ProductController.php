<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductPhoto;
use Symfony\Component\HttpFoundation\Session\Session;
use File;
use Modules\Category\Entities\Category;
use Modules\MainCategory\Entities\MainCategory;
use Modules\SubCategory\Entities\SubCategory;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->paginate(config('number_of_pages'));

        $main_categories = MainCategory::orderBy('main_category')->get();

        $categories = Category::orderBy('category_name')->get();

        return view('product::admin.index',
            compact('main_categories', 'products','categories')
        );
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('product::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //

        $rules = [
            'title' => 'required',
            'subcategory_id' => 'required',
            'price' => 'required',
            'email' => 'required|email',
            'ad_number' => 'required',
            'photo.*' => 'mimes:jpg,jpeg,gif,png',
            'video' =>'mimes:flv,mp4,3gp,wmv,dat,avi,mov,mkv'
        ];

        $names = [
            'subcaetgory_id' => 'Sub Category'

        ];

        $request->validate($rules, [], $names);


        $product = new Product;
        $product->title = $request->title;
        $product->subcategory_id = $request->subcategory_id;
        $product->price = $request->price;
        $product->condition = $request->condition;
        $product->mississippi = $request->mississippi;
        $product->contact_name = $request->contact_name;
        $product->email = $request->email;
        $product->ad_number = $request->ad_number;
        $product->website = $request->website;
        $product->location = $request->location;
        $product->slug = md5($product->title) . date('ymdhis') . rand(0, 9999);

        // Upload the video

        if ($request->has('video')) {
            $video = $request->file('video');
            $time = date('YmdHis');
            $videoName = $time . '-' . $video->getClientOriginalName();
            $video->move(public_path() . '/videos/', $videoName);
            $product->video = $videoName;
        }
        $product->save();

        //if photo is selected upload and save photos
        if ($request->has('photo')) {

            //  dd($request->photo);

            foreach ($request->file('photo') as $item) {
                $product_photo = new ProductPhoto();
                $time = date('YmdHis');
                $imageName = $time . '-' . $item->getClientOriginalName();
                //$path =
                $item->move(public_path() . '/product/', $imageName);
                $product_photo->photo = $imageName;
                $product_photo->product_id = $product->id;
                $product_photo->save();
            }
        }





        return redirect()->route('product.index');
    }



    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {

        $product = Product::where('slug', $id)->first();
        $subcategories = SubCategory::all();

        $product_photos = ProductPhoto::where('product_id', $product->id)->get();
        return view(
            'product::admin.edit',
            compact('product', 'subcategories', 'product_photos')
        );
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {

        $product = Product::where('slug', $id)->first();


        $rules = [
            'title' => 'required',
            'subcategory_id' => 'required',
            'price' => 'required',
            'email' => 'required|email',
            'ad_number' => 'required',
            'photo.*' => 'mimes:jpg,jpeg,gif,png',
            'video' =>'mimes:flv,mp4,3gp,wmv,dat,avi,mov,mkv,ogg'
        ];

        $names = [
            'subcaetgory_id' => 'Sub Category'

        ];

        $request->validate($rules, [], $names);

        $product->product_name = $request->product_name;
        $product->title = $request->title;
        $product->subcategory_id = $request->subcategory_id;
        $product->price    = $request->price;
        $product->condition    = $request->condition;
        $product->mississippi = $request->mississippi;
        $product->contact_name    = $request->contact_name;
        $product->email    = $request->email;
        $product->ad_number    = $request->ad_number;
        $product->website    = $request->website;
        $product->location = $request->location;
        $product->slug = md5($product->title) . date('ymdhis') . rand(0, 9999);


        // Video upload
         // Upload the video

         if ($request->has('video')) {
             //old video
             $old_video = public_path('videos/').$product->video;

            //  dd($old_video);

             if(File::exists($old_video)){
                File::delete($old_video);
             }

            $video = $request->file('video');
            $time = date('YmdHis');
            $videoName = $time . '-' . $video->getClientOriginalName();
            $video->move(public_path() . '/videos/', $videoName);
            $product->video = $videoName;
        }

        $product->save();

        //if photo is selected upload and save photos

        if ($request->has('photo')) {

            //  dd($request->photo);

            foreach ($request->file('photo') as $item) {
                $product_photo = new ProductPhoto();
                $time = date('YmdHis');
                $imageName = $time . '-' . $item->getClientOriginalName();
                //$path =
                $item->move(public_path() . '/product/', $imageName);
                $product_photo->photo = $imageName;
                $product_photo->product_id = $product->id;
                $product_photo->save();
            }
        }

        return redirect()->route('product.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */


    public function delete(Request $request)
    {
        if ($request->slug) {
            $products = Product::whereIn('slug', $request->slug)->get();

            foreach ($products as $value) {

                $old_video = public_path('videos/').$value->video;

                if(File::exists($old_video)){
                   File::delete($old_video);
                }
                $product_photos = ProductPhoto::where('product_id', $value->id)->get();



                foreach ($product_photos as $key => $v) {

                     // unlink the photos
                    $old_path = public_path() . '/product/' . $v->photo;

                    if (File::exists($old_path)) {
                        File::delete($old_path);
                    }
                }
                //delete the product
                $value->delete();

            }
        }


        return back();
    }


    /*
    ..........................................................................................
        DELETE pHOTO
    ..........................................................................................
    */
    public function deletePhoto($id)
    {

        $photo = ProductPhoto::find($id);

        $product = Product::find($photo->product_id);

        $slug = $product->slug;

        //unlink the photo

        $old_path = public_path() . '/product/' . $photo->photo;

        if (File::exists($old_path)) {
            File::delete($old_path);
        }

        //delete the record from the database
        $photo->delete();


        return redirect()->route('product.edit', $slug);
    }


    /*
    Search
     */

    public function search(Request $request){

        $keyword = $request->keyword;
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $product = Product::where(function($q) use ($keyword){
            $q->where('product_name', 'like', '%' .$keyword . '%')
              ->orWhere('title', 'like', '%' .$keyword. '%')
              ->orWhere('price', 'like', '%' .$keyword. '%')
              ->orWhere('condition', 'like', '%' .$keyword. '%')
              ->orWhere('mississippi', 'like', '%' .$keyword. '%')
              ->orWhere('email', 'like', '%' .$keyword. '%')
              ->orWhere('ad_number', 'like', '%' .$keyword. '%')
              ->orWhere('website', 'like', '%' .$keyword. '%')
              ->orWhere('country', 'like', '%' .$keyword. '%')
              ->orWhere('state', 'like', '%' .$keyword. '%')
              ->orWhere('city', 'like', '%' .$keyword. '%')
              ->orWhere('location', 'like', '%' .$keyword. '%');

        });

        if($date_from && $date_to){
            $date_from = strtotime($request->date_from);
            $date_to = strtotime($request->date_to);
            $product = $product->whereBetween('created_at',[$date_from,$date_to]);
        }

        $product = $product->get();

        return response()->json($product);

    }

    /*
        Load Category
    */

    public function getCategory($id){
        $category = Category::where('main_category_id',$id)->get();

        return response()->json($category);
    }

    /* load subcategory */

    public function getSubCategory($id){
        $subcategory = SubCategory::where('category_id',$id)->get();

        return response()->json($subcategory);
    }
}
