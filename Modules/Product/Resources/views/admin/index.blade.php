@extends('layouts.admin.main')




@section('title')
Products
@endsection
@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Products
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- search -->

            {{-- @include('bank::common.search') --}}
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link @if(!session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#Users"> Products
                            </a></li>
                        @if(in_array('product.store',session('permission')))

                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Add New product
                            </a></li>
                        @endif
                    </ul>
                    <div class="tab-content mt-0">
                        <div class="tab-pane @if(!session()->get('errors')) active show @endif" id="Users">
                            <div class="table-responsive">

                                <form method="post">
                                    @csrf
                                    @if(in_array('product.delete',session('permission')))

                                    <button class="btn btn-danger"
                                        onclick="return confirm('Do you really want to delete this item?')"
                                        formaction="{{ route('product.delete') }}" type="submit">
                                <i class="fa fa-trash"></i>
                                </button>

                                @endif
                                <table class="table table-hover table-custom spacing8">
                                    <thead>
                                        <tr>
                                            @if(in_array('product.delete',session('permission')))

                                            <th>
                                                <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" id="check_all">
                                                    <span>#</span>
                                                </label>
                                            </th>
                                            @endif
                                            <th>product</th>

                                            @if(in_array('product.edit',session('permission')))

                                            <th>Actions</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($products as $value)

                                        <tr>
                                            @if(in_array('product.delete',session('permission')))

                                                <td>

                                                    <label class="fancy-checkbox">
                                                        <input class="checkbox-tick checkitem" type="checkbox" name="slug[]"
                                                            value="{{ $value->slug }}">
                                                        <span></span>
                                                    </label>
                                                </td>
                                            @endif

                                            <td>
                                                <h6 class="mb-0"> {{$value->title}}</h6>
                                            </td>

                                            @if(in_array('product.edit',session('permission')))


                                            <td>
                                                <a href="{{route('product.edit',$value->slug)}}">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>

                                            @endif

                                            @endforeach
                                    </tbody>
                                </table>
                                </form>
                                {{-- {!! $Products->render() !!} --}}

                            </div>
                        </div>

                        @if(in_array('product.store',session('permission')))

                            <div class="tab-pane @if(session()->get('errors')) active show @endif" id="addUser">
                                <div class="body mt-2">

                                    @include('product::admin.create')

                                </div>

                                @if($errors->any())
                                {!! implode('', $errors->all("<div class='error'>
                                    <p style='color:red'>:message</p>
                                </div>")) !!}
                                @endif
                            </div>

                        @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>



<script src="{{ asset('public/js/preview_image.js') }}">

</script>
@endsection
