@extends('layouts.admin.main')




@section('title')
Edit Product
@endsection


@section('content')

<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row clearfix">
                <div class="col-md-6 col-sm-12">
                    <h2>Edit Product
                    </h2>
                </div>
            </div>
        </div>

        <div class="row clearfix">
            <!-- search -->

            {{-- @include('bank::common.search') --}}
            <div class="col-lg-12">
                <div class="card">
                    <ul class="nav nav-tabs">

                        </a></li>
                        <li class="nav-item"><a class="nav-link @if(session()->get('errors')) active show @endif"
                                data-toggle="tab" href="#addUser">Edit Product
                            </a></li>
                    </ul>
                    <div class="tab-content mt-0">

                        <div class="tab-pane active show" id="addUser">
                            <div class="body mt-2">


                                <div class="row clearfix">


                                    <form action="{{route('product.update',$product->slug)}}"
                                        enctype="multipart/form-data" method="post">

                                        @method('PUT')

                                        @csrf
                                        <div class="row clearfix">

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">

                                                            <label for="product_name"
                                                                class="input-group-text">Product</label>
                                                        </div>
                                                        <input type="text" name="product_name" id="product_name"
                                                            class="form-control show-tick @error('product_name') is-invalid @enderror"
                                                            placeholder="product *"
                                                            value="{{old('product_name',$product->product_name)}}"
                                                            required="">
                                                    </div>
                                                </div>
                                            </div>

                                            @error('product_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="title" class="input-group-text">Title</label>
                                                        </div>
                                                        <input type="text" name="title" id="title"
                                                            class="form-control show-tick @error('title') is-invalid @enderror"
                                                            placeholder="product *"
                                                            value="{{old('title',$product->title)}}" required="">
                                                    </div>
                                                </div>
                                            </div>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                            {{-- sub categories --}}


                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="subcategory_id" class="input-group-text">Sub
                                                                Category</label>
                                                        </div>
                                                        <select name="subcategory_id" id="subcategory_id"
                                                            class="form-control">
                                                            <option value=""></option>
                                                            @foreach ($subcategories as $value)
                                                            <option value="{{ $value->id }}"
                                                                {{ old('subcategory_id',$product->subcategory_id)==$value->id? 'selected' : ''  }}>
                                                                {{ $value->subcategory_name }}
                                                            </option>

                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>




                                            {{-- Price --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="price" class="input-group-text">Price</label>
                                                        </div>
                                                        <input type="text" name="price" id="price"
                                                            class="form-control show-tick @error('price') is-invalid @enderror"
                                                            placeholder="Price *"
                                                            value="{{old('price',$product->price)}}" required="">
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- Condition --}}
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">


                                                        <label>Condition</label> &nbsp;&nbsp;

                                                        <label class="fancy-radio custom-color-green"><input
                                                                name="condition" value="New" type="radio"
                                                                @if($product->condition=='New') checked
                                                            @endif><span><i></i>New</span></label>
                                                        <label class="fancy-radio custom-color-green"><input
                                                                name="condition" value="Used" type="radio"
                                                                @if($product->condition=='Used') checked
                                                            @endif><span><i></i>Used</span></label>
                                                    </div>
                                                </div>
                                            </div>


                                            {{-- Mississippi --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">


                                                        <label>Mississippi</label> &nbsp;&nbsp;

                                                        <label class="fancy-radio custom-color-green">
                                                            <input name="mississippi" value="Mr" type="radio"
                                                                @if($product->mississippi=='Mr') checked @endif
                                                            ><span><i></i>Mr</span>
                                                        </label>
                                                        <label class="fancy-radio custom-color-green">
                                                            <input name="mississippi" value="Mrs" type="radio"
                                                                @if($product->mississippi=='Mrs') checked
                                                            @endif><span><i></i>Mrs</span>
                                                        </label>
                                                        <label class="fancy-radio custom-color-green">
                                                            <input name="mississippi" value="Ms" type="radio"
                                                                @if($product->mississippi=='Ms') checked
                                                            @endif><span><i></i>Ms</span>
                                                        </label>
                                                        <label class="fancy-radio custom-color-green">
                                                            <input name="mississippi" value="Other" type="radio"
                                                                @if($product->mississippi=='Other') checked
                                                            @endif><span><i></i>Other</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>




                                            {{-- Price --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="contact_name" class="input-group-text">Contact
                                                                Name</label>
                                                        </div>
                                                        <input type="text" name="contact_name" id="contact_name"
                                                            class="form-control show-tick @error('price') is-invalid @enderror"
                                                            placeholder="Contact Name *"
                                                            value="{{old('contact_name',$product->contact_name)}}"
                                                            required="">
                                                    </div>
                                                </div>
                                            </div>


                                            {{-- email --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="email" class="input-group-text">Email</label>
                                                        </div>
                                                        <input type="email" name="email" id="email"
                                                            class="form-control show-tick @error('email') is-invalid @enderror"
                                                            placeholder="Email *"
                                                            value="{{old('email',$product->email)}}" required="">
                                                    </div>
                                                </div>
                                            </div>



                                            {{-- Advertisement number --}}


                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="ad_number" class="input-group-text">Ad
                                                                Number</label>
                                                        </div>
                                                        <input type="text" name="ad_number" id="ad_number"
                                                            class="form-control show-tick @error('ad_number') is-invalid @enderror"
                                                            placeholder="Ad Number *"
                                                            value="{{old('ad_number',$product->ad_number)}}"
                                                            required="">
                                                    </div>
                                                </div>
                                            </div>


                                            {{-- Website --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="website"
                                                                class="input-group-text">Website</label>
                                                        </div>
                                                        <input type="text" name="website" id="website"
                                                            class="form-control show-tick @error('website') is-invalid @enderror"
                                                            placeholder="Website "
                                                            value="{{old('website',$product->website)}}">
                                                    </div>
                                                </div>
                                            </div>



                                            {{-- Location --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="location"
                                                                class="input-group-text">Location</label>
                                                        </div>
                                                        <input type="text" name="location" id="location"
                                                            class="form-control show-tick @error('location') is-invalid @enderror"
                                                            placeholder="Location "
                                                            value="{{old('location',$product->location)}}" required="">
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- photo upload --}}



                                            <div class="col-12 photo">
                                                <div class="form-group mt-3">
                                                    <input type="file" name="photo[]" multiple id="photo"
                                                        class="dropify" onchange="preview_image();">

                                                    <small id="fileHelp" class="form-text text-muted">to select multiple
                                                        files, hold down the CTRL or SHIFT key while selecting..</small>
                                                </div>
                                            </div>

                                            <div class="col-12 photo">
                                                <div class="form-group">
                                                    <div id="image_preview"></div>

                                                </div>
                                            </div>



                                            @if($product_photos->isEmpty()==false)
                                            <div class="col-12 photo">
                                                <div class="form-group mt-3 mb-5">
                                                    @foreach ($product_photos as $value)
                                                    <img height="100px"
                                                        src="{{ asset('public/product/'.$value->photo) }}" />
                                                    &nbsp; &nbsp;

                                                    <a href="{{ route('delete.photo',$value->id) }}"
                                                        class="btn btn-danger"
                                                        onclick="return confirm('Are You Sure Want To Delete This Photo?')">
                                                        <i class="fa fa-trash"></i>
                                                    </a>

                                                    @endforeach
                                                </div>
                                            </div>

                                            @endif



                                            {{-- Video upload --}}

                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">


                                                            <label for="video" class="input-group-text">Featured
                                                                Video</label>
                                                        </div>
                                                        <input type="file" name="video" id="video" class="form-control">
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <video width="320" controls>
                                                        <source src="{{ asset('public/videos/'.$product->video) }}"
                                                            type="video/mp4">
                                                        {{-- <source src="movie.ogg" type="video/ogg"> --}}
                                                        Your browser does not support the video tag.
                                                    </video>
                                                </div>
                                            </div>


                                            <div class="col-12">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                                <a class="btn btn-secondary" href="{{route('product.index')}}">
                                                    CLOSE
                                                </a>
                                            </div>

                                        </div>

                                    </form>








                                </div>
                                @if($errors->any())
                                {!! implode('', $errors->all("<div class='error'>
                                    <p style='color:red'>:message</p>
                                </div>")) !!}
                                @endif
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var count=0;
    function preview_image()
    {
        var total_file=document.getElementById("photo").files.length;
        for(var i=0;i<total_file;i++)
        {
            $('#image_preview').append(` <div id='image${count}'>
            <img height='100px' src='${URL.createObjectURL(event.target.files[i])}'>
                        <a  href ='#' onclick='removeFile(${count})' ></a> </div>`);




                        count++;
        }


    }


    // function removeFile(i) {

    //     var total_file=document.getElementById("photo").value;

    //     console.log("file object",total_file);


    //     document.getElementById("file_index").value=arrays;

    //     $('#image'+i).remove();
    // }
</script>
@endsection
