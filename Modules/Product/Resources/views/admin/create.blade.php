<form action="{{route('product.store')}}" enctype="multipart/form-data" method="post">
    @csrf
    <div class="row clearfix">

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">

                        <label for="product_name"
                            class="input-group-text">Product</label>
                    </div>
                    <input type="text" name="product_name" id="product_name"
                        class="form-control show-tick @error('product_name') is-invalid @enderror"
                        placeholder="product *" value="{{old('product_name')}}"
                        required="">
                </div>
            </div>
        </div>

        @error('product_name')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="title" class="input-group-text">Title</label>
                    </div>
                    <input type="text" name="title" id="title"
                        class="form-control show-tick @error('title') is-invalid @enderror"
                        placeholder="product *" value="{{old('title')}}" required="">
                </div>
            </div>
        </div>

        @error('title')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        {{-- Maincategory --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <label for="main_category_id" class="input-group-text">Main Category</label>
                    </div>
                    <select name="main_category_id" id="main_category_id"
                        class="form-control">
                        <option value="">--Select Main Category--</option>

                        @foreach ($main_categories as $value)
                        <option value="{{ $value->id }}">
                            {{ $value->main_category }}
                        </option>
                    </select>
                </div>
            </div>
        </div>


        {{-- Category url --}}

        <input type="hidden" id ="category_url" name="category_url" value="{{ url('product/get-category') }}">

        {{-- Category --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="category_id" class="input-group-text">
                            Category</label>
                    </div>
                    <select name="category_id" id="category_id"
                        class="form-control">
                        <option value="">--Category--</option>
                    </select>
                </div>
            </div>
        </div>

        {{-- Sub Category url --}}

        <input type="hidden" id="subcategory_url" name="sub_category_url" value="{{ url('product/get-subcategory') }}">

        {{-- sub categories --}}


        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="subcategory_id" class="input-group-text">Sub
                            Category</label>
                    </div>
                    <select name="subcategory_id" id="subcategory_id"
                        class="form-control">
                        <option value=""></option>


                        @endforeach
                    </select>
                </div>
            </div>
        </div>




        {{-- Price --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="price" class="input-group-text">Price</label>
                    </div>
                    <input type="text" name="price" id="price"
                        class="form-control show-tick @error('price') is-invalid @enderror"
                        placeholder="Price *" value="{{old('price')}}" required="">
                </div>
            </div>
        </div>

        {{-- Condition --}}
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">


                    <label>Condition</label> &nbsp;&nbsp;

                    <label class="fancy-radio custom-color-green"><input
                            name="condition" value="New"
                            type="radio"><span><i></i>New</span></label>
                    <label class="fancy-radio custom-color-green"><input
                            name="condition" value="Used" type="radio"
                            checked><span><i></i>Used</span></label>
                </div>
            </div>
        </div>


        {{-- Mississippi --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">


                    <label>Mississippi</label> &nbsp;&nbsp;

                    <label class="fancy-radio custom-color-green">
                        <input name="mississippi" value="Mr" type="radio"
                            checked><span><i></i>Mr</span>
                    </label>
                    <label class="fancy-radio custom-color-green">
                        <input name="mississippi" value="Mrs"
                            type="radio"><span><i></i>Mrs</span>
                    </label>
                    <label class="fancy-radio custom-color-green">
                        <input name="mississippi" value="Ms"
                            type="radio"><span><i></i>Ms</span>
                    </label>
                    <label class="fancy-radio custom-color-green">
                        <input name="mississippi" value="Other"
                            type="radio"><span><i></i>Other</span>
                    </label>
                </div>
            </div>
        </div>




        {{-- Price --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="contact_name" class="input-group-text">Contact
                            Name</label>
                    </div>
                    <input type="text" name="contact_name" id="contact_name"
                        class="form-control show-tick @error('price') is-invalid @enderror"
                        placeholder="Contact Name *" value="{{old('contact_name')}}"
                        required="">
                </div>
            </div>
        </div>


        {{-- email --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="email" class="input-group-text">Email</label>
                    </div>
                    <input type="email" name="email" id="email"
                        class="form-control show-tick @error('email') is-invalid @enderror"
                        placeholder="Email *" value="{{old('email')}}" required="">
                </div>
            </div>
        </div>



        {{-- Advertisement number --}}


        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="ad_number" class="input-group-text">Ad
                            Number</label>
                    </div>
                    <input type="text" name="ad_number" id="ad_number"
                        class="form-control show-tick @error('ad_number') is-invalid @enderror"
                        placeholder="Ad Number *" value="{{old('ad_number')}}"
                        required="">
                </div>
            </div>
        </div>


        {{-- Website --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="website" class="input-group-text">Website</label>
                    </div>
                    <input type="text" name="website" id="website"
                        class="form-control show-tick @error('website') is-invalid @enderror"
                        placeholder="Website " value="{{old('website')}}">
                </div>
            </div>
        </div>



        {{-- Location --}}

        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="location" class="input-group-text">Location</label>
                    </div>
                    <input type="text" name="location" id="location"
                        class="form-control show-tick @error('location') is-invalid @enderror"
                        placeholder="Location " value="{{old('location')}}" required="">
                </div>
            </div>
        </div>

        {{-- photo upload --}}



        <div class="col-12 photo">
            <div class="form-group mt-3 mb-5">
                <input type="file" name="photo[]" multiple id="photo" class="dropify"
                    onchange="preview_image();">

                <small id="fileHelp" class="form-text text-muted">to select multiple
                    files, hold down the CTRL or SHIFT key while selecting..</small>
            </div>
        </div>

        <div class="col-12 photo">
            <div class="form-group">
                <div id="image_preview"></div>

            </div>
        </div>


         {{-- Video upload --}}

         <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">


                        <label for="video" class="input-group-text">Featured Video</label>
                    </div>
                    <input type="file" name="video" onchange="selectedVideo(this);"
                        class="form-control upload-video-file" >
                </div>
            </div>
        </div>

        {{-- Video preview --}}
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="form-group">
                <div class='video-prev' class="pull-right">
                    <video height="200" width="300" id="video"  controls="controls">
                        <source class="video-preview" id="source" type="video/mp4">

                    </video>
                </div>
            </div>
        </div>


        <div class="col-12">
            <button type="submit" class="btn btn-primary">Add</button>
            <a class="btn btn-secondary" href="{{route('product.index')}}">
                CLOSE
            </a>
        </div>

    </div>


    {{-- removed files --}}


{{-- <input type="hidden" name="file_index[]" id="file_index">  --}}

</form>
