<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('subcategory_id');
            $table->foreign('subcategory_id')->references('id')
                    ->on('sub_categories')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->string('price');
            $table->string('condition'); //new used 

            // contact details
            $table->string('mississippi'); //mr mrs miss others
            $table->string('contact_name');
            $table->string('email');
            $table->string('ad_number');
            $table->string('website')->nullable();
            $table->string('location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
