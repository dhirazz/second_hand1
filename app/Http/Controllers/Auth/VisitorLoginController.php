<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
// use Illuminate\Routing\Controller;

use App\Http\Controllers\Controller;
use App\Visitor;
// use Illuminate\Support\Facades\Auth;
use Auth;
use Laravel\Socialite\Facades\Socialite;

class VisitorLoginController extends Controller
{



    public function __construct()
    {
        $this->middleware('guest:visitor');
    }

    public function showLoginForm()
    {
        return view('auth.visitor-login');
    }

    public function login(Request $request)
    {
        // validate the form \data_

        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];

        $request->validate($rules);

        // attempt to log the user in
        $login = Auth::guard('visitor')->attempt(
            ['email' => $request->email, 'password' => $request->password],
            $request->remember
        );

        // dd($login);

        if ($login) {

            // if successful then redirect to their intended location 
            return redirect()->intended(route('visitor.dashboard'));
        }

        // if unsuccessful then redirect back to the form with data
        return redirect()->back()->withInput($request->only('email'));
    }


    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $github_user = Socialite::driver('github')->user();
        
        $visitor = Visitor::where('provider_id', $github_user->getId())->first();

        if (!$visitor) {
            $user = Visitor::create([
                'email' =>  $github_user->getEmail(),
                'name' => $github_user->GetNickname(),
                'provider_id' => $github_user->getId()

            ]);

            $login = Auth::guard('visitor')->login($user, true);
        } else {
            $login = Auth::guard('visitor')->login($visitor, true);
        }

      



        // if successful then redirect to their intended location 
        return redirect()->intended(route('visitor.dashboard'));

        // return redirect('')




        // $user->token;
    }


    // logout for visitor
    public function logout(Request $request)
    {
        Auth::guard('visitor')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect()->guest(route('visitor.login'));
    }
}
