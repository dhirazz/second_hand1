<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Auth;


class VisitorController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:visitor');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route('front.page');
    }



    // Logout for visitor
    public function logout(Request $request)
    {
        // $this->guard('visitor')->logout();

        // Auth::logout();
        // return redirect('/login');

        $this->guard('visitor')->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect()->route('front.page');

    }
}
