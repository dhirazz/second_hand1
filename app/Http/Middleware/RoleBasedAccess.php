<?php

namespace App\Http\Middleware;

use Closure;

use App\User;



use Illuminate\Contracts\Session\Session as SessionSession;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Modules\Role\Entities\Role;
use Modules\Role\Entities\RolePermission;
use Session;


class RoleBasedAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role_id = auth()->user()->role_id;



        //check if role is private or public
        $role = Role::find($role_id);

        if ($role->type == 'superadmin') {


            // routes for superadmin
            $route_name = [];


            foreach (Route::getRoutes()->getRoutes() as $route) {
                $action = $route->getAction();
                if (array_key_exists('as', $action)) {
                    $route_name[] = $action['as'];
                }
            }

            $role_permission = $route_name;

           // dd($role_permission);

            Session::put('permission', $role_permission);

            //dd(session('permission'));
        } else {

            $request_route = Route::currentRouteName();

        

           

                $role_permission = RolePermission::select('link')
                    ->join('permissions', 'role_permissions.permission_id', '=', 'permissions.id')
                    ->where('role_id', $role_id)->pluck('link')
                    ->all();


                $allowed_routes = [
                    // '/',
                    // 'home',
                    // 'user/view-profile',
                    // 'user/update-profile',
                    // 'user/post-change-password'

                    'admin',
                ];

                $role_permission = array_merge($role_permission, $allowed_routes);
            



            // if there is edit route then add update route also

            foreach ($role_permission as  $value) {
               
           

                if (strpos($value, 'edit') == true) {
                    $arr = explode('.', $value);

                    if (@$arr) {
                        $module_name = $arr[0];

                        $new_update_route = $module_name . '.' . 'update';
                        array_push($role_permission, $new_update_route);
                    }
                }
            }


            // for making allow the update for route that route that contains edit route



            Session::put('permission', $role_permission);


            if (!in_array($request_route, Session('permission'))) {
                return redirect('access-forbidden');
            }

        }




        return $next($request);
    }
}
