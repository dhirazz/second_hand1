<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;
use Modules\Category\Entities\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         // category sub category in header
       view()->composer('layouts.front.header', function($view){

        $categories = Category::orderBy('category_name')->get();
        $settings =  Setting::first();

     
        $view->with(compact('categories','settings'));
    });

  

    // settings in footer
    view()->composer('layouts.front.footer', function($view){


        $settings =  Setting::first();
        $categories = Category::orderBy('category_name')->get();

     
        $view->with(compact('settings','categories'));
    });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        config(['number_of_pages' =>10]);

    }
}
